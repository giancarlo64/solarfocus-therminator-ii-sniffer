#if __STDC_VERSION__ >= 199901L
#define _XOPEN_SOURCE 600
#else
#define _XOPEN_SOURCE 500
#endif /* __STDC_VERSION__ */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <termios.h>    // POSIX terminal control definitions
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <curses.h>
#include <sys/ioctl.h>
#include <mysql/mysql.h>
#include <time.h>
#include "minIni.h"
#include "solarfocus1.h"
#include "lifo_DB.h"
#include <math.h>
#include <syslog.h>


/*******************************************************************************************/
/*                                                                                         */
/*  Filename:  solarfocus_data_sniffer.c                                                   */
/*  Author:    G.C.D.                                                                      */
/*  Version:   see gitlab                                                                  */
/*  Data:      11.10.2018                                                                  */
/*  ---------------------------------------------------------------------------------------*/
/*  A. Function:                                                                           */
/*     RS485 Data Sniffing of Solarfocus Therminator II                                    */
/*                                                                                         */
/*  B. References:                                                                         */
/*     None                                                                                */
/*                                                                                         */
/*  C. Specifications:                                                                     */
/*     None                                                                                */
/*                                                                                         */
/*  D. Comment:                                                                            */
/*     Beta version                                                                        */
/*                                                                                         */
/*  E. Modification History                                                                */
/*                 V0.1  First Draft for extracting data from file. Lots of bugs           */
/*                 V0.2  Corrected the bugs                                                */
/*     31/03/2017  V0.4  copy file in memory to pointer easily                             */
/*     01/04/2017  V0.5  changed the type of finding the messages.                         */
/*                       look at the values 0x02 and not on the len of msg                 */
/*     04/04/2017  V0.51 deleted the not needed code                                       */
/*     06/04/2017  V0.6  Started decoding the msg data                                     */
/*     05/08/2017  V0.62 Formatting of data to be printed for recognition                  */
/*                 V0.63 some minor changes on the description                             */
/*     11/08/2017  V0.64 Added text colors for better identification                       */
/*                 V0.64_variant      some minor modification                              */
/*     14/08/2017  V0.65 Reformatting data for better viewing                              */
/*                       added header file solarfocus.h                                    */
/*     15/08/2017  V0.7  Code optimization with macro                                      */
/*     15/08/2017  V0.71 Grouped message header macro (master and slave)                   */
/*                       in a single additional macro                                      */
/*     16/08/2017  V0.72 Corrected solar flow speed                                        */
/*                       saved data in fixed point format (1 decimal) and not before       */
/*                       as floating point (save computation power)                        */
/*     20/08/2017  V0.73 added options parameters in the program call                      */
/*                       translated all the messages into English                          */
/*     21/08/2017  V0.74 added message "use at your own risk"                              */
/*                 V0.75 More data decoded                                                 */
/*                 V0.76 Started coding for saving data in database                        */
/*     05/12/2017  V0.77 Code for writing database finished                                */
/*     09/12/2017  V0.78 added filter saving database only when selected value changed     */
/*                       created a function that sync on a Master Msg and resync in        */
/*                       case there is a error on the length without exiting               */  
/*     10/12/2017  V0.79 Solved problem with loosing sync (to many row data saved          */
/*                       in DB per second) caused USB serial buffer to go overflow         */  
/*     11/12/2017  V0.80 Fixed problem saving negative numbers in the DB                   */
/*                       INT data was saved instead of short                               */
/*     12/12/2017  V0.81 Add INI configuration file with reading through minIni            */
/*     13/12/2017  V0.82 Codified the valve's position by extracting the ON-OFF bit        */
/*                       from the solar PUMP 1                                             */
/*     18/12/2017  V0.83 Correct error decoded or deleted errors. The negative values      */
/*                       were not saved correctly. Now save them in decimal and not in HEX */
/*     20/12/2017  V0.84 Decode master set point output temperature message                */
/*                       PID and DIV PID values of fresh water. Added to DB                */ 
/*                       Decoded I3 value of FW (ON-OFF) and added to DB                   */
/*     26/01/2018 V0.85  Voltage risc. PWM decoded.                                        */
/*     03/10/2018 V0.86  added the ability to disable the save of RAW data in DV (ini file)*/
/*     11/10/2018 V0.87  First realease available to all on gitlab                         */ 
/*                       From now all the versioning detail are on gitlab                  */                                  
/*******************************************************************************************/


const char inifile[] = "solarfocus_config.ini";


/******************************************************************************************************************/
/*                       Solarfocus description of my installation                                                */
/******************************************************************************************************************/

//	S1 = Solar regulator 1 (main regolator)
//	S2 = Solar regulator 2

//      S1Ix = Input signal number X  for solar regulator 1
//      S1Ox = Output signal number X from solar regulator 1 

// DESCRIPTION OF THE SOLAR SYSTEM 
	// 2 Buffer of 1000l connected in series 
	// Buffer 1 output: high temperature closed circuit
	// Buffer 1 input: Output of Buffer 2
	// Buffer 2 input of low temperature closed circuit 

    // SENSORS AND ACTUATORS CONNECTED TO THE BUFFERS 
	// Buffer 1 (main buffer): 2 solar registers
		// S1I2	Signal for switching the valve between upper and lower solar register
		// X39 	Upper temperature: 
		// S1I9	Middle temperature of upper solar register
		// X44	Lower temperature of upper solar Register
		// X36	Middle temperature of Buffer (upper temperature of lower solar register)
		// S1I4	Lower temperature of lower solar register

	// Buffer 2: only 1 lower solar register
		// S2I9	Lower temperature of lower solar register
		// X35	Lower temperature of the buffer 2

	// Solar Collectors
		// S1I1	Collector temperature


/***************************************************************************************************************************/
/*                                   PROTOCOL   DESCRIPTION                                                                */
/***************************************************************************************************************************/

	// here we will describe the format and content of the msg on the RS485 BUS
	// In the RS485 BUS the Master is the ECO-Manager Touch PC (touchpanel) and all the other devices are the slaves
	// In my installation there re the followings slaves:
	//		Therminator II Motherboard: Address 0x11
	//		Solar regulator 1 (main regulator):  Address 0x13
	//              Solar Regulator 2 : Address 0x14
	//              Freshwater Unit:  Address 0x17 

	// GENERAL DESCRIPTION
	// Every mgs starts with Byte value 0x02
	// If the second Byte is different from 0x02 and from 0x00 so the msg it sended from the master of the BUS
	// If the second Byte is 0x02 so the msg it is sended from a SLAVE
	// If the BYTE to be send is a 0x02 the device introduce after it a 0x00 value (escape) 
		// to differenciate it from the start of msg (0x02). If the receiver detect 0x02 0x00 sequence it konw that this is not a start of msg
 		// and it must delete the 0x00 to reproduce the right received msg


	// Structure of a Master Msg

	// Byte 1:  0x02
	// Byte 2:  msg len (must be different from 0x02 and 0x00)
	// Byte 3:  slave ID (Indicate to witch slave the msg is addressed)
	//	In my installation: 	
	//			0x11  Motherboard
	//			0x13  Solarzentrale 1 (Address 2)
	//			0x14  Solarzentrale 2 (Address 3)
	//			0x17  Frischwasserzentrale (Address 5)
	// Byte 4:  Incremental value from 64 to 127


	//  Structure of a Slave Msg
        
	//  Byte 1  0x02
        //  Byte 2  0x02
        //  Byte 3  msg len
        //  Byte 4  Master Addr         0x10
        //  Byte 5  Counter value  from 64 to 127
        //  Byte 6  allways             0x80
        //  Byte 7  Same value as Byte 5 of Master (Master command)

// function that extract msg from the serial port
// return the whole master message including 0x02 STX
// In slave msg the first 0x02 will be deleted so that the len correspond to the msg returned.
int getMsg(int USB_PORT, unsigned char* Msg)
{
	int msg_found = FALSE;
	int msg_type = Master_ID;
	static int old_msg_type=100; // 100 detect fist time in fuction
	char temp;
	short i;


    read(USB_PORT, &Msg[0], 1);

	while (!msg_found)
	{
		read(USB_PORT, &Msg[1], 1);
		if ((Msg[0] == 0x02) && (Msg[1] != 0x00))  msg_found=TRUE; // begin of master-msg found
		else Msg[0]=Msg[1];
	}
    if (Msg[0] == Msg[1]) // slave msg found save lenght in the second array position
    {
		read(USB_PORT, &Msg[1], 1);  
		msg_type = Slave_ID;
	}
	for (i=2; i< Msg[1]; i++) 
	{
		read(USB_PORT, &Msg[i], 1);
		if (Msg[i]== 0x02) read(USB_PORT, &temp, 1);  // don't save escape value
	}
	if (old_msg_type == msg_type) syslog( LOG_DEBUG, " Found 2 same Msg Types: %d",msg_type);
	old_msg_type = msg_type;
	return msg_type;
}


/*****************************************************************************************************/
/*                         REVERSE ENGINEERING OF THE MESSAGES                                       */
/*****************************************************************************************************/

	// Master MSG for solar Data

	// Byte 1:  0x02
    // Byte 2:  msg len
    // Byte 3:  slave ID (msg for with slave)
    //      In my case:     
    //                      0x13  Solarzentrale 1 (Address 2)
    //                      0x14  Solarzentrale 2 (Address 3)
    // Byte 4:  Incremental value (from 64 to 127)
	// Byte 5: 
	// Byte 6: bit 0: 0: Pump OFF  1: Pump ON
	// Byte 7:
	// Byte 8: 0...100 (0..10V OUT1 pump speed regulation)	


	
int save_solar_master1_msg_data(unsigned char *bus_msg, SavedStruct *data, unsigned short *DB_val)
{
	int bus_msg_len;
	int msg_len=0;
	int i;
	
	save_header_Master();
	save_1_byte_no_conversion("NOT YET DECODED",1,0);

    data->Msg_Value_LSB = *bus_msg;
    DB_val[S1_O1_Pump_State] = (data->Msg_Value_LSB & Pump_Solar1_MASK);
    data->Real_Value = 0;
	data->divisor = 1;
	strcpy(data->Description, "Solar Pump State:  0=OFF 1=ON");
	data->un_valore = TRUE;
	data->conversione = FALSE;
    data++;
    msg_len++;
    // DO NOT INCREMENT BUS_MSG !!!! (SAME MSG FOR THE NEXT DECODE
    data->Msg_Value_LSB = *bus_msg;
    DB_val[S1_O2_Valve_State] = ((data->Msg_Value_LSB & VALVE_MASK)>>1);
    data->Real_Value = 0;
	data->divisor = 1;
	strcpy(data->Description, "Solar Valve Position:  0=LOWER REG 1=UPPER REG");
	data->un_valore = TRUE;
	data->conversione = FALSE;
    data++;
    msg_len++;	
	bus_msg++;
		
	save_1_byte_no_conversion("NOT YET DECODED",1,0);	
	save_1_byte_no_conversion("Solar Pump speed:  0...100% = 0...10V",1,S1_OUT1_Pump_SET_Speed);				
		
	for (i=8; i<(bus_msg_len-1); i++) 
	{	
		save_1_byte_no_conversion("NOT YET DECODED",1,0);
	}
	
	save_Checksum();
	
	return msg_len;
}

int save_solar_master2_msg_data(unsigned char *bus_msg, SavedStruct *data, unsigned short *DB_val)
{
	int bus_msg_len;
	int msg_len=0;
	int i;
	
	save_header_Master();
	save_1_byte_no_conversion("NOT YET DECODED",1,0);
	save_1_byte_no_conversion("Solar Pump State:  0=OFF 1=ON",1,S2_O1_Pump_State);	
	save_1_byte_no_conversion("NOT YET DECODED",1,0);	
	save_1_byte_no_conversion("Solar Pump speed:  0...100% = 0...10V",1,S2_OUT1_Pump_SET_Speed);				
		
	for (i=8; i<(bus_msg_len-1); i++) 
	{	
		save_1_byte_no_conversion("NOT YET DECODED",1,0);
	}
	
	save_Checksum();
	
	return msg_len;
}


// SLAVE  solar 1 (Address 0x13)
    //  Byte 1  	0x02
    //  Byte 2  	0x02
    //  Byte 3  	msg len
    //  Byte 4  	Master Addr         0x10
    //  Byte 5  	Same value as Byte 4 of master
    //  Byte 6  	allways             0x80
    //  Byte 7  	Same value as Byte 5 of Master (Master command)
	//  Byte 11+12	Temperature I1 (Sensor S1I1) (value in tenths of a degree): MSB Byte 11 Temperatura collettori
	//  Byte 13+14  Temperature I2 (Sensor S1I2) (value in tenths of a degree): MSB Byte 13
	//  Byte 15+16  Temperature I3 (Sensor S1I3) (value in tenths of a degree): MSB Byte 15 Mandata solare
	//  Byte 17+18  Temperature I4 (Sensor S1I4) (value in tenths of a degree): MSB Byte 17 
	//  Byte 19+20  Temperature I5 (Sensor S1I5) (value in tenths of a degree): MSB Byte 19 Ritorno solare
	//  Byte 21+22  Temperature I6 (Sensor S1I6) (value in tenths of a degree): MSB Byte 21
	//  Byte 23+24  Temperature I7 (Sensor S1I7) (value in tenths of a degree): MSB Byte 23
	//  Byte 25+26  Temperature I8 (Sensor S1I8) (value in tenths of a degree): MSB Byte 25
	//  Byte 27+28  Temperature I9 (Sensor S1I9) (value in tenths of a degree): MSB Byte 27
	//  Byte 29+30  Temperature I10 (Sensor S1I10) (value in tenths of a degree): MSB Byte 29
	//  Byte 31+32  Temperature I11 (Sensor S1I11) (value in tenths of a degree): MSB Byte 31
	//  Byte 33+34  Temperature I12 (Sensor S1I12) (value in tenths of a degree): MSB Byte 33
	//  Byte 35+36  Temperature I13 (Sensor S1I13) (value in tenths of a degree): MSB Byte 35
	//  Byte 37+38  Temperature I14 (Sensor S1I14) (value in tenths of a degree): MSB Byte 37
	//  Byte 39+40
	//  Byte 41+42  Voltage (0..10V) of pump regulation S1 OUT1 (value in 0.01V steps)
	//  Byte 43+44
	//  Byte 45+46  Speed of the solar fluid: The value is the elapsed time between two pluse in 100th of seconds
	//		(value 1 = 0.01 s). To compute the l/h ->  0.5*100*3600/(float)(slave_msg[44]*256+slave_msg[45])
		    					    					    
					    
int save_solar_slave1_msg_data(unsigned char *bus_msg, SavedStruct *data, unsigned short *DB_val)
{
	int bus_msg_len;
	int msg_len=0;
	int i;
	static int Last_Flow_elapsed_time_value=0;
	static time_t last_timestamp=0;


	save_header_Slave();
	
	save_1_byte_no_conversion("NOT YET DECODED",1,0);				
	save_1_byte_no_conversion("NOT YET DECODED",1,0);
	save_1_byte_no_conversion("NOT YET DECODED",1,0);					
	save_2_byte_with_conversion("Temperature S1I1 solar collector",10,S1_I1);	
	save_2_byte_with_conversion("Temperature S1I2 (not connected)",10,0);
	save_2_byte_with_conversion("Temperature S1I3 solar collector",10,S1_I3);
	save_2_byte_with_conversion("Temperature S1I4 solar collector",10,S1_I4);
	save_2_byte_with_conversion("Temperature S1I5 solar collector",10,S1_I5);
	save_2_byte_with_conversion("Temperature S1I6 (not connected)",10,0);
	save_2_byte_with_conversion("Temperature S1I7 (not connected)",10,0);
	save_2_byte_with_conversion("Temperature S1I8 (not connected)",10,0);
	save_2_byte_with_conversion("Temperature S1I9",10,S1_I9);
	save_2_byte_with_conversion("Temperature S1I10 (not connected)",10,0);
	save_2_byte_with_conversion("Temperature S1I11 (not connected)",10,0);
	save_2_byte_with_conversion("Temperature S1I12 (not connected)",10,0);
	save_2_byte_with_conversion("Temperature S1I13 (not connected)",10,0);
	save_2_byte_with_conversion("Temperature S1I14 (not connected)",10,0);
	save_2_byte_with_conversion("NOT YET DECODED",10,0);	
	save_2_byte_with_conversion("Regulation Voltage of the pump Nr.1 in tenth of Volt",10,S1_Pump_CURRENT_Speed);
	save_2_byte_with_conversion("NOT YET DECODED",10,0);	
	
	data->Msg_Value_MSB = *bus_msg;
	bus_msg++;
	data->Msg_Value_LSB = *bus_msg;
	// ten times grater to have a resolution of 0.1 liter 0.5/impulse is now 5l/impulse
	data->Real_Value =  (short)(5*100*3600/(float)(data->Msg_Value_MSB*256+data->Msg_Value_LSB));
	data->divisor = 10;
	strcpy(data->Description, "Zin encoder velocita solare l/h");
    DB_val[Flow_elapsed_time] = data->Real_Value;
	data->un_valore = FALSE;
    data->conversione = TRUE;	                 
	bus_msg++;
	data++;
	msg_len++;
	
	// i=45 instead of 46 ... it works but it must be verified why 	    										
	for (i=45; i<(bus_msg_len-1); i++) 
	{	
		save_1_byte_no_conversion("NOT YET DECODED",1,0);
	}
					
	save_Checksum();
	
	// compute the actual produced power from solar
	// Energia = massa per calore_specifico * delta T
	// P[W] = 0.5 l/impuls * 1/Flow_elapsed_time * (T_S1_I5 - TS1_I3) * 1000 * 4.186
	// P[W] = l/hour (=DB_val[Flow_elapsed_time]) * (T_S1_I5 - TS1_I3) *1.16278  <- (1000 * 4.186/3600)
	
	if (last_timestamp == 0)   // fist time in function
	{
		last_timestamp = time(NULL) - MAX_TIME_FLOW;
		Last_Flow_elapsed_time_value = DB_val[Flow_elapsed_time];
		DB_val[Flow_elapsed_time]=0;
		
	}
	else
	{
		if (Last_Flow_elapsed_time_value == DB_val[Flow_elapsed_time])
		{
			if ((time(NULL) - last_timestamp) > MAX_TIME_FLOW) DB_val[Flow_elapsed_time]=0;
		}
		else
		{
			last_timestamp = time(NULL);
			Last_Flow_elapsed_time_value = DB_val[Flow_elapsed_time];
			
		}
		DB_val[Solar_instant_power] = (short) (DB_val[Flow_elapsed_time]/10.0 * (DB_val[S1_I5]- DB_val[S1_I3])/10.0 * 1.16278);
	}
	
	msg_len++;
	
	return msg_len;
}


// SLAVE  solar 2 (Address 0x14)
        //  Byte 1      0x02
        //  Byte 2      0x02
        //  Byte 3      msg len
        //  Byte 4      Master Addr         0x10
        //  Byte 5      Same value as Byte 4 of master
        //  Byte 6      allways             0x80
        //  Byte 7      Same value as Byte 5 of Master (Master command)
        //  Byte 11+12  Temperature I1 (Sensor S2I1) Not connected
        //  Byte 13+14  Temperature I2 (Sensor S2I2) 
        //  Byte 15+16  Temperature I3 (Sensor S2I3) 
        //  Byte 17+18  Temperature I4 (Sensor S2I4) 
        //  Byte 19+20  Temperature I5 (Sensor S2I5) 
        //  Byte 21+22  Temperature I6 (Sensor S2I6) 
        //  Byte 23+24  Temperature I7 (Sensor S2I7) 
        //  Byte 25+26  Temperature I8 (Sensor S2I8) 
        //  Byte 27+28  Temperature I9 (Sensor S2I9)  CONNECTED
        //  Byte 29+30  Temperature I10 (Sensor S1I10) 
        //  Byte 31+32  Temperature I11 (Sensor S1I11) 
        //  Byte 33+34  Temperature I12 (Sensor S1I12) 
        //  Byte 35+36  Temperature I13 (Sensor S1I13) 
        //  Byte 37+38  Temperature I14 (Sensor S1I14) 
        //  Byte 39+40 
        //  Byte 41+42  Voltage (0..10V) of pump regulation S2 OUT1(value in 0.01V of precision)
	
				
int save_solar_slave2_msg_data(unsigned char *bus_msg, SavedStruct *data, unsigned short *DB_val)
{
	int bus_msg_len;
	int msg_len=0;
	int i;

	save_header_Slave();
	
	save_1_byte_no_conversion("NOT YET DECODED",1,0);				
	save_1_byte_no_conversion("NOT YET DECODED",1,0);
	save_1_byte_no_conversion("NOT YET DECODED",1,0);
	save_2_byte_with_conversion("Temperature S2I1",1,0);	
	save_2_byte_with_conversion("Temperature S2I2 (not connected)",10,0);
	save_2_byte_with_conversion("Temperature S2I3 (not connected)",10,0);
	save_2_byte_with_conversion("Temperature S2I4 (not connected)",10,0);
	save_2_byte_with_conversion("Temperature S2I5 (not connected)",10,0);
	save_2_byte_with_conversion("Temperature S2I6 (not connected)",10,0);
	save_2_byte_with_conversion("Temperature S2I7 (not connected)",10,0);
	save_2_byte_with_conversion("Temperature S2I8 (not connected)",10,0);
	save_2_byte_with_conversion("Temperature S2I9",10,S2_I9);
	save_2_byte_with_conversion("Temperature S2I10 (not connected)",10,0);
	save_2_byte_with_conversion("Temperature S2I11 (not connected)",10,0);
	save_2_byte_with_conversion("Temperature S2I12 (not connected)",10,0);
	save_2_byte_with_conversion("Temperature S2I13 (not connected)",10,0);
	save_2_byte_with_conversion("Temperature S2I14 (not connected)",10,0);
	save_2_byte_with_conversion("NOT YET DECODED",10,0);	
	save_2_byte_with_conversion("Regulation Voltage of the pump Nr.2 in tenth of Volt",10,S2_Pump_CURRENT_Speed);	
					
	// i=41 instead of 42 ... it works but it must be verified why 	    										
	for (i=41; i<(bus_msg_len-1); i++) 
	{	
		save_1_byte_no_conversion("NOT YET DECODED",10,0);
	}
					
	save_Checksum();
	
	return msg_len;
}

// Master MSG for Motherboard

    // Byte 1:  0x02
    // Byte 2:  msg len 
    // Byte 3:  slave ID (msg for with slave)
    //      In my case:  0x11 
    // Byte 4:  Incremental value from 64 to 127
    // Byte 5:  Allways 0x58
    // Byte 6:  
    //           bit7    
    //           bit6   
    //           bit5   
    //           bit4   X15 Standard back(return) and protection pump
    //           bit3   
    //           bit2   X4  Ingiction hot hair 1=enabled 0=disabled
    //           bit1   X26 primary air magnet 1=enabled 0=disabled 
    //           bit0 
    // Byte 7:
    // Byte 8:
    //           bit7   X16 ash extraction  1=enabled 0=disabled 
    //           bit6   
    //           bit5   
    //           bit4   
    //           bit3   
    //           bit2   X17 secondary air motor control 1=OPENING 0=CLOSING (bit 1 must be enabled)
    //           bit1   X17 secondary air motor control 1=enabled 0=disabled 
    //           bit0 
    // Byte 9:
    //           bit7  
    //           bit6   
    //           bit5   X13 Boiler return mixer control 1=OPENING 0=CLOSING (bit 4 must be enabled)
    //           bit4   X13 Boiler return mixer control 1=enabled 0=disabled 
    //           bit3   
    //           bit2   
    //           bit1   
    //           bit0  
    // Byte 10:
    // Byte 11:
    // Byte 12: FAN comando caldaia :  94=ON  00=OFF 
    // Byte 13: Tens. Risc. PWM





int save_Motherboard_master_msg_data(unsigned char *bus_msg, SavedStruct *data, unsigned short *DB_val)
{
	int bus_msg_len;
	int msg_len=0;
	int i;
	
	save_header_Master();
	save_1_byte_no_conversion("NOT YET DECODED",1,0); //Byte 5
// BYTE 6
	data->Msg_Value_LSB = *bus_msg;
    	DB_val[X26_primary_air_control] = ((data->Msg_Value_LSB & PRIMARY_AIR_MASK)>>1);
    	data->Real_Value = 0;
	data->divisor = 1;
	strcpy(data->Description, "Primary AIR MAgnet:  0=OFF 1=ON");
	data->un_valore = TRUE;
	data->conversione = FALSE;
    	data++;
    	msg_len++;
    	// DO NOT INCREMENT BUS_MSG !!!! (SAME MSG FOR THE NEXT DECODE
    	
	data->Msg_Value_LSB = *bus_msg;
    	DB_val[X4_hot_air_injection] = ((data->Msg_Value_LSB & HOT_AIR_INJECTION_MASK)>>2);
   	data->Real_Value = 0;
	data->divisor = 1;
	strcpy(data->Description, "Hot Air Injection:  0=OFF 1=ON");
	data->un_valore = TRUE;
	data->conversione = FALSE;
    	data++;
    	msg_len++;	
        // DO NOT INCREMENT BUS_MSG !!!! (SAME MSG FOR THE NEXT DECODE
        
        data->Msg_Value_LSB = *bus_msg;
        DB_val[X15_standard_return_pump] = ((data->Msg_Value_LSB & STANDARD_RETURN_PUMP_MASK)>>4);
        data->Real_Value = 0;
        data->divisor = 1;
        strcpy(data->Description, "Standard return pump:  0=OFF 1=ON");
        data->un_valore = TRUE;   
        data->conversione = FALSE;
        data++;
        msg_len++;      
        bus_msg++;

	save_1_byte_no_conversion("NOT YET DECODED",1,0); //BYTE 7

// BYTE 8
        data->Msg_Value_LSB = *bus_msg;
	if (data->Msg_Value_LSB & SECONDARY_AIR_ENABLE_MASK)
        	DB_val[X17_secondary_air_control] = (((~(data->Msg_Value_LSB) & SECONDARY_AIR_MASK)>>1)-1); //2->1 and 0->-1
	else DB_val[X17_secondary_air_control] = 0;
        data->Real_Value = 0;
        data->divisor = 1;
        strcpy(data->Description, "Secondary air control :  -1= CLOSING 0=OFF 1=OPENING");
        data->un_valore = TRUE;   
        data->conversione = FALSE;
        data++;
        msg_len++;      
        // DO NOT INCREMENT BUS_MSG !!!! (SAME MSG FOR THE NEXT DECODE
        
        data->Msg_Value_LSB = *bus_msg;
        DB_val[X16_ash_extraction] = ((data->Msg_Value_LSB & ASH_EXTRACTION_MASK)>>7);
        data->Real_Value = 0;
        data->divisor = 1;
        strcpy(data->Description, "ASH Extraction:  0=OFF 1=ON");
        data->un_valore = TRUE;   
        data->conversione = FALSE;
        data++;
        msg_len++;      
        bus_msg++;

// BYTE 9

	data->Msg_Value_LSB = *bus_msg;
        if (data->Msg_Value_LSB & BOILER_MIXER_ENABLE_MASK)
                DB_val[X13_boiler_return_mixer] = (((data->Msg_Value_LSB & BOILER_MIXER_MASK)>>4)-2); //3->1 and 1->-1
        else DB_val[X13_boiler_return_mixer] = 0;
        data->Real_Value = 0;
        data->divisor = 1;
        strcpy(data->Description, "Boler Return Mixer :  -1= CLOSING 0=OFF 1=OPENING");
        data->un_valore = TRUE;   
        data->conversione = FALSE;
        data++;
        msg_len++;
	bus_msg++;

        save_1_byte_no_conversion("NOT YET DECODED",1,0); //BYTE 10
	save_1_byte_no_conversion("NOT YET DECODED",1,0); //BYTE 11
	// BYTE 12
	save_1_byte_no_conversion("FAN control boiler:  94=ON  00=OFF",1,Fan_Control_Boiler);
	save_1_byte_with_conversion("Tens. Risc. PWM",1,Tens_Risc_PWM);		
		
	for (i=13; i<(bus_msg_len-1); i++) 
	{	
		save_1_byte_no_conversion("NOT YET DECODED",1,0);
	}
					
	save_Checksum();
	
	return msg_len;
}


// SLAVE  Motherboard (Address 0x11)
	//  Byte 1      0x02
    //  Byte 2      0x02
    //  Byte 3      msg len
    //  Byte 4      Master Addr         0x10
    //  Byte 5      Same value as Byte 4 of master
    //  Byte 6      allways             0x80
    //  Byte 7      Same value as Byte 5 of Master (Master command)
    //  Byte 8
    //  Byte 9
    //  Byte 10
    //  Byte 11
	//  Byte 12+13 Temperatura fumi (Sensor X34) ERRATO BISOGNA EFFETTUARE UNA CORREZIONE (sopra i 100 gradi) (value in tenths of a degree): MSB Byte 12 
    //  Byte 14+15  (Sensor X30) (value in tenths of a degree): MSB Byte 14
    //  Byte 16+17  Temperatura caldaia (Sensor X31) (value in tenths of a degree): MSB Byte 16
    //  Byte 18+19  Temperatura ritorno (Sensor X32) (value in tenths of a degree): MSB Byte 18 Mandata solare
    //  Byte 20+21  
    //  Byte 22+23  Tensione trasformatore di riscaldamento (in centesimi di volt)
    //  Byte 24+25  Corrente trasformatore di riscaldamento (in centesimi di ampere)
    //  Byte 26+27  Temperature X35 (Sensor X35) (value in tenths of a degree):  DA VERIFICARE
    //  Byte 28+29  (Sensor X40)
    //  Byte 30+31  Riscaldamento (Sensor X38) (value in tenths of a degree): MSB Byte 30
    //  Byte 32+33  Not used (Sensor X41) (value in tenths of a degree): MSB Byte 32
    //  Byte 34+35  Sensore mandata riscaldamento 2 o pompa di ricircolo (Sensor X37 or X43) 
    //  Byte 36+37  X36 (Sensor X36) (value in tenths of a degree):
    //  Byte 38+39  Temperature X44 (Sensor X44) (value in tenths of a degree):
    //  Byte 40+41  Temperature X39 (Sensor X39) (value in tenths of a degree):
    //  Byte 42+43  External Temperature X42 (Sensor X42) (value in tenths of a degree):
    //  Byte 44+45  
	//  Byte 46+47  Sensore mandata riscaldamento 2 o pompa di ricircolo (Sensor X37 or X43)
	//  Byte 48+49   Tensione sonda Lambda signed (16bit) 
	//  Byte 50+51
	//  Byte 52+53
	//  Byte 54+55 
    //  Byte 56+57 
    //  Byte 58+59
	//  Byte 60+61 
    //  Byte 62+63 
    //  Byte 64+65
    //  Byte 66         Velocita ventola caldaia NON DEFINITO A COSA CORRISPONDE
    //  Byte 67   	        
    //  Byte 68+69
    //  Byte 70+71
    //  Byte 72+73 
    //  Byte 74+75 
    //  Byte 76+77 
    //  Byte 78+79 
    //  Byte 80+81 
    //  Byte 82+83
	//  Byte 84  CRC ?  


int save_Motherboard_slave_msg_data(unsigned char *bus_msg, SavedStruct *data, unsigned short *DB_val)
{
	int bus_msg_len;
	int msg_len=0;
	int i;
    short SmokeVal;

	save_header_Slave();
	
	save_1_byte_no_conversion("NOT YET DECODED",1,0);				
	save_1_byte_no_conversion("NOT YET DECODED",1,0);
	save_1_byte_no_conversion("NOT YET DECODED",1,0);
	save_1_byte_no_conversion("NOT YET DECODED",1,0);
	save_smoke_2_byte_with_conversion("Smoke Temperature: IS calibrated",10,X34);	//Temperatura fumi (Sensor X34) CALIBRATO MA DA VERIFICARE
	save_2_byte_with_conversion("Sensor External boiler (X30) NOT USED",10,X30);					
	save_2_byte_with_conversion("Boiler Temperature Sensor(X31)",10,X31); //Temperatura caldaia 	
	save_2_byte_with_conversion("Return flow temperature sensor(X32)",10,X32);
	save_2_byte_with_conversion("NOT YET DECODED",10,0);
	save_2_byte_with_conversion("Heating Transormator Voltage (V)",100,U_Heating_Trafo);
	save_2_byte_with_conversion("Heating Transformator Current (A)",100,I_Heating_Trafo);
	save_2_byte_with_conversion("Optional buffer temperature sensor(X35)  TO BE VERIFIED",10,X35);	
	save_2_byte_with_conversion("Sensor X40",10,X40);
	save_2_byte_with_conversion("Flow temperature sensor heating circuit 1(X38)",10,X38);
	save_2_byte_with_conversion("Not used (Sensor X41) ",10,0);
	save_2_byte_with_conversion("Heating flow sensor 2 or recirculation pump (Sensor X37 or X43)  not used",10,X37);	//Sensore mandata riscaldamento 2 o pompa di ricircolo (Sensor X37 or X43)  not used
	save_2_byte_with_conversion("Buffer temperature Sensor bottom (X36)",10,X36);
	save_2_byte_with_conversion("Buffer temperature sensor TOP (X44)",10,X44);
	save_2_byte_with_conversion("DrinkWater buffer temperature sensor (X39)",10,X39);
	save_2_byte_with_conversion("Outdoor Sensor (X42)",10,X42);
	save_2_byte_with_conversion("NOT YET DECODED",10,0);
	save_2_byte_with_conversion("Heating flow sensor 2 or recirculation pump (Sensor X37 or X43)  not used",10,X37_1);	//Sensore mandata riscaldamento 2 o pompa di ricircolo (Sensor X37 or X43)  not used
	
	
	data->Msg_Value_MSB = *bus_msg;
	bus_msg++;
	data->Msg_Value_LSB = *bus_msg;
	data->Real_Value = (data->Msg_Value_MSB*256+data->Msg_Value_LSB);
	strcpy(data->Description, "Voltage of Lambda sensor signed (16bit)");
	DB_val[U_Lambda] = data->Real_Value;
	data->divisor = 10;
	data->un_valore = FALSE;        	                 
	bus_msg++;
	data++;	
	msg_len++;	
	
	for (i=0; i<18; i++) 
	{	
		save_1_byte_no_conversion("NOT YET DECODED",1,0);
	}
	
	save_1_byte_no_conversion("Boiler FAN Speed: VALUE NOT YET DECODED",1,Fan_Speed); //Velocita ventola caldaia NON DEFINITO A COSA CORRISPONDE
	
	
	// i=67 instead of 68 ... it works but it must be verified why 	    										
	for (i=67; i<(bus_msg_len-1); i++) 
	{	
		save_1_byte_no_conversion("NOT YET DECODED",1,0);
	}
					
	save_Checksum();
	
	return msg_len;
}



	
// Master MSG for Frischwassermodul

	// Byte 1:  0x02
    // Byte 2:  msg len
    // Byte 3:  slave ID (msg for with slave)
    //      In my case:   0x17 
    // Byte 4:  Increment value referred to the slave msg
	// Byte 5:
	// Byte 6:
	// Byte 7:
	// Byte 8: Velocita' (in %) impostata nella pompa acqua sanitaria quando azionata manualmente (tramite touch)	
	// Byte 9: Complemento della velocita' (in %) (100% - Byte8)  impostata nella pompa acqua sanitaria	"manuale"
	// Byte 10:
	// Byte 11:
	// Byte 12:
	// Byte 13:
	// Byte 14:
	// Byte 15:
	// Byte 16:
	// Byte 17:
	// Byte 18:
	// Byte 19:
	// Byte 20+21: Valore P del regolatore PID
	// Byte 22+23: Valore I del regolatore PID
	// Byte 24+25: Valore D del regolatore PID
	// Byte 26+27: Setpoint della temperatura di uscita dello scambiatore 
	// Byte 28:
	// Byte 29: Checksum


int save_Freshwater_master_msg_data(unsigned char *bus_msg, SavedStruct *data, unsigned short *DB_val)
{
	int bus_msg_len;
	int msg_len=0;
	int i;
	
	save_header_Master();
	
	save_1_byte_no_conversion("NOT YET DECODED",1,0);
	save_1_byte_no_conversion("NOT YET DECODED",1,0);
	save_1_byte_no_conversion("NOT YET DECODED",1,0);
	save_1_byte_with_conversion("Speed (in \%) Fresh Water Manual Pump command",1,Fresh_Water_Pump_Manual_SET_Speed);
	save_1_byte_no_conversion("Speed (100\%- speed \%) Fresh Water Pump",1,0);
	
	for (i=9; i<18; i++) 
	{	
		save_1_byte_no_conversion("NOT YET DECODED",1,0);
	}
	save_1_byte_with_conversion("Sampling Time Fresh Water reg",10,Fresh_Water_Sampling_Time);
	save_2_byte_with_conversion("P Val of PID reg.",1,FW_P_of_PID);
	save_2_byte_with_conversion("I Val of PID reg.",1,FW_I_of_PID);
	save_2_byte_with_conversion("D Val of PID reg.",1,FW_D_of_PID);
	save_2_byte_with_conversion("Output T. SetPoint",10,Fresh_Water_Temp_SetPoint);	
	
	save_1_byte_no_conversion("NOT YET DECODED",1,0);
	save_1_byte_no_conversion("NOT YET DECODED",1,0);
	
	save_2_byte_with_conversion("DIV P Val of PID reg.",1,FW_DIV_P_of_PID);
	save_2_byte_with_conversion("DIV I Val of PID reg.",1,FW_DIV_I_of_PID);
	save_2_byte_with_conversion("DIV D Val of PID reg.",1,FW_DIV_D_of_PID);
	save_2_byte_with_conversion("DIV LIMIT P Val of PID reg.",1,FW_LIMIT_I_of_PID);
	
				
	for (i=37; i<(bus_msg_len-1); i++) 
	{	
		save_1_byte_no_conversion("NOT YET DECODED",1,0);
	}
					
	save_Checksum();
	
	return msg_len;
}


// SLAVE  Frischwassermodul (Adrress 0x17)
        //  Byte 1      0x02
        //  Byte 2      0x02
        //  Byte 3      msg len
        //  Byte 4      Master Addr         0x10
        //  Byte 5      Same value as Byte 4 of master
        //  Byte 6      allways             0x80
        //  Byte 7      Same value as Byte 5 of Master (Master commannd)
        //  Byte 11+12  
        //  Byte 13+14   
        //  Byte 15+16  Sonda I3: Entrata analogica con segnale digitale 
        //              Sensore: STS01-DC
        //              -30 (gradi) indica cortocircuito quindi acqua scorre
        //              +130 (gradi) indica circuito aperto (R alta) acqua ferma 
        //              quindi decodificare sengale analogico in digitale 0-1  
        //  Byte 17+18   
        //  Byte 19+20  
        //  Byte 21+22   
        //  Byte 23+24   
        //  Byte 25+26  
        //  Byte 27+28  
        //  Byte 29+30  
        //  Byte 31+32  
        //  Byte 33+34   
        //  Byte 35+36  
        //  Byte 37+38  Temperatura uscita scambiatore di calore (verso la doccia) 
        //  Byte 39+40 
        //  Byte 41+42  


int save_freshwater_slave_msg_data(unsigned char *bus_msg, SavedStruct *data, unsigned short *DB_val)
{
	int bus_msg_len;
	int msg_len=0;
	int i;

	save_header_Slave();
	
	save_1_byte_no_conversion("NOT YET DECODED",1,0);				
	save_1_byte_no_conversion("NOT YET DECODED",1,0);
	save_1_byte_no_conversion("NOT YET DECODED",1,0);
	save_1_byte_no_conversion("NOT YET DECODED",1,0);
	save_1_byte_no_conversion("NOT YET DECODED",1,0);
	save_1_byte_no_conversion("NOT YET DECODED",1,0);
	save_1_byte_no_conversion("NOT YET DECODED",1,0);

	
	data->Msg_Value_MSB = *bus_msg;
	bus_msg++;
	data->Msg_Value_LSB = *bus_msg;
	if( (short)(data->Msg_Value_MSB*256+data->Msg_Value_LSB) > 30) /* SENSOR detectiong no water flow*/
		data->Real_Value = 0;
	else data->Real_Value = 1; /* SENSOR detectiong water flow*/
	DB_val[Fresh_Water_Flowing] = data->Real_Value;
	data->divisor = 1;
	strcpy(data->Description, "I3 Sensor Water flowing: Converted to ON-OFF");
	data->un_valore = FALSE;
	data->conversione = TRUE;
	bus_msg++;
	data++;
	msg_len++;
        
        
	
	for (i=0; i<20; i++) 
	{	
		save_1_byte_no_conversion("NOT YET DECODED",1,0);
	}
	
	save_2_byte_with_conversion("Heat Exchanger Output Temperature (Sanitary Water Part of exchanger)",10,Fresh_Water_OUT_Temperature); //Temperatura uscita scambiatore di calore (verso la doccia) 
	
	// i=37 instead of 38 ... it works but it must be verified why 	    										
	for (i=37; i<(bus_msg_len-1); i++) 
	{	
		save_1_byte_no_conversion("NOT YET DECODED",1,0);
	}
					
	save_Checksum();
	
	return msg_len;
}


static struct termios save_termios;
static int ttysavefd = -1;
static enum
{
	RESET,
	RAW, 
	CBREAK,
}
ttystate= RESET;




int tty_raw(int fd)     // put terminal into a raw mode
{
    int             err;
    struct termios  buf;

    if (ttystate != RESET) {
        errno = EINVAL;
        endwin();
        return(-1);
    }
    if (tcgetattr(fd, &buf) < 0)
    {
		endwin();
        return(-1);
	}
    save_termios = buf; //structure copy

    // Echo off, canonical mode off, extended input
    // processing off, signal chars off.
    buf.c_lflag &= ~(ECHO | ICANON | IEXTEN | ISIG);

    // No SIGINT on BREAK, CR-to-NL off, input parity
    // check off, don't strip 8th bit on input, output
    // flow control off.
    buf.c_iflag &= ~(BRKINT | ICRNL | INPCK | ISTRIP | IXON);

    // Clear size bits, parity checking off.
    buf.c_cflag &= ~(CSIZE | PARENB);

    // Set 8 bits/char.
    buf.c_cflag |= CS8;
    
    // Output processing off.
    buf.c_oflag &= ~(OPOST);

    // Case B: 1 byte at a time, no timer.
    buf.c_cc[VMIN] = 1;
    buf.c_cc[VTIME] = 0;
    cfsetispeed(&buf,B19200);

    if (tcsetattr(fd, TCSAFLUSH, &buf) < 0)
    {
		syslog( LOG_CRIT, " ERROR setting terminal attributes");
		endwin();
        return(-1);
	}

    // Verify that the changes stuck. tcsetattr can return 0 on
    // partial success.
    if (tcgetattr(fd, &buf) < 0) 
    {
        err = errno;
        tcsetattr(fd, TCSAFLUSH, &save_termios);
        errno = err;
        endwin();
        syslog( LOG_CRIT, " ERROR setting terminal attributes");
        return(-1);
    }
    if ((buf.c_lflag & (ECHO | ICANON | IEXTEN | ISIG)) ||
      (buf.c_iflag & (BRKINT | ICRNL | INPCK | ISTRIP | IXON)) ||
      (buf.c_cflag & (CSIZE | PARENB | CS8)) != CS8 ||
      (buf.c_oflag & OPOST) || buf.c_cc[VMIN] != 1 ||
      buf.c_cc[VTIME] != 0) 
    {
		// Only some of the changes were made. Restore the
        // original settings.
        tcsetattr(fd, TCSAFLUSH, &save_termios);
        errno = EINVAL;
        endwin();
        syslog( LOG_CRIT, " ERROR setting terminal attributes");
        return(-1);
    }

    ttystate = RAW;
    ttysavefd = fd;
    endwin();
    return(0);
}

void formatted_print(SavedStruct *Msg, int Msg_len, int H_position)
{
	int i;

	for (i=4; i< Msg_len+4; i++)
	{
		move(i, H_position);
		refresh();
		if (Msg->un_valore)
		{
			if (Msg->conversione == FALSE)
			{
				printw(" %03d", Msg->Msg_Value_LSB);
				move(i, H_position+offset1);
				refresh();
				printw("¦  NA");
				move(i, H_position+offset2);
				refresh();
				printw("¦ %s", Msg->Description);
			}				
			else
			{
				printw(" %03d", Msg->Msg_Value_LSB);
				move(i, H_position+offset1);
				refresh();
				printw("¦             ");
				move(i, H_position+offset1);
				refresh();
				/* print the values from fixed point format (1 decimal) in "floating" point */
				/* if value <100 print only one decimal digit */
				if (Msg->Real_Value >= 100)
				printw("¦   %3d.%2d",Msg->Real_Value/Msg->divisor,Msg->Real_Value%Msg->divisor);
				else printw("¦   %3d.%1d",Msg->Real_Value/Msg->divisor,Msg->Real_Value%Msg->divisor);
				move(i, H_position+offset2);
				refresh();
				printw("¦ %s", Msg->Description);
			}
		}
		else
		{
			if (Msg->conversione == FALSE)
			{
				printw(" %03d  %03d",Msg->Msg_Value_MSB, Msg->Msg_Value_LSB);
				move(i, H_position+offset1);
				refresh();
				printw("¦  NA");
				move(i, H_position+offset2);
				refresh();
				printw("¦ %s", Msg->Description);
			}				
			else
			{
				printw(" %03d %03d",Msg->Msg_Value_MSB, Msg->Msg_Value_LSB);
				move(i, H_position+offset1);
				refresh();
				printw("¦             ");
				move(i, H_position+offset1);
				refresh();
				/* print the values from fixed point format (1 decimal) in "floating" point */
				printw("¦   %3d.%02d",Msg->Real_Value/Msg->divisor,(Msg->Real_Value%Msg->divisor)*Msg->divisor);
				move(i, H_position+offset2);
				refresh();
				printw("¦ %s", Msg->Description);
			}
		}
		Msg++;					
	}
}

void printHeader(int H_position, char *string)
{
	// print header
	move(0, H_position);
	refresh();
		
	printw(string);
	move(1, H_position);
	refresh();
	printf(" ---------------------------------------------------");
	move(2, H_position);
	refresh();
    printw(" Msg Value");
    move(2, H_position + offset1);
	refresh();
    printw("¦  Value");
    move(2, H_position + offset2);
	refresh();
    printw("¦ Description");
    move(3, H_position);
	refresh();
    printw(" ----------------------------------------------------");
    
}

// get actual time to save in DB
int timespec2str(char *buf, unsigned int len, struct timespec *ts) {
    int ret;
    struct tm t;

    tzset();
    if (localtime_r(&(ts->tv_sec), &t) == NULL)
        return -1;

	
    ret = strftime(buf, len, "'%Y-%m-%d %H:%M:%S", &t);
    if (ret == 0)
        return -2;
    len -= ret - 1;

    ret = snprintf(&buf[strlen(buf)], len, ".%03ld',", (ts->tv_nsec)/NANO_TO_MILLISECOND);
    if (ret >= len)
        return -3;

    return strlen(buf);
}



int decoded_data_string(unsigned short* Value, char* Output_Msg)
{
	int i,val;
	struct timespec ts;
	clockid_t clk_id = CLOCK_REALTIME;
	
    clock_gettime(clk_id, &ts);
	
	val = timespec2str(Output_Msg, DB_STR_LEN, &ts);
	if (val<0)
	{	
		return -1;
	}
	Output_Msg+= val;
		
	for(i=S1_O1_Pump_State; i< LAST_VALUE_FOR_INDEX; i++)
	{
		val=sprintf(Output_Msg, "%hd,", Value[i]);
		Output_Msg+= val;
	}
	*(--Output_Msg) = 0; // delete comma from the last value and insert EOS 
	return 1;
	
}


void finish_with_error(MYSQL *con)
{
  fprintf(stderr, "%s\n", mysql_error(con));
  syslog( LOG_CRIT, " ERROR QUERY TO DB: %s",mysql_error(con));
  mysql_close(con);    
}


void save_row_data_DB(Therminator_II_Slave Device, char* Master_Msg, char* Slave_Msg, DB_Info* Solarfocus)
{
	MYSQL conn;
	int val;
    char Time_Msg[100];
    static Saved_DB_String *p_first=NULL;
	static Saved_DB_String *p_last=NULL;
	
	struct timespec ts;
	
	mysql_init(&conn);
	
	
	clockid_t clk_id = CLOCK_REALTIME;
	
    clock_gettime(clk_id, &ts);
    
    val = timespec2str(Time_Msg, DB_STR_LEN, &ts);
    
    
    char statement[DB_STR_MAX_LEN];
    
    snprintf(statement, 2048, "INSERT INTO Raw_Data VALUES (%s%d,X'%s',X'%s')", Time_Msg, Device, Master_Msg, Slave_Msg);
    
    /* Connect to database */
   if (!mysql_real_connect(&conn, Solarfocus->DB_server, Solarfocus->DB_user, Solarfocus->DB_password, Solarfocus->DB_name, Solarfocus->DB_port, NULL, 0)) 
    {
		syslog( LOG_CRIT, " ERROR CONNECTING TO DB FOR RAW DATA: ERROR CODE %u: %s",mysql_errno(&conn),mysql_error(&conn));
       
		save_data_in_dB_buffer(statement, &p_first, &p_last);
		syslog( LOG_DEBUG, "Actual Nr of Raw Msg in dB-buffer %d",dB_buffer_len(p_first));
		return;
    }
	if (mysql_query(&conn, statement))
	{
		finish_with_error(&conn);
		syslog( LOG_CRIT, " ERROR SAVING RAW DATA in DB");
		save_data_in_dB_buffer(statement, &p_first, &p_last);
		syslog( LOG_DEBUG, "Actual Nr of Raw Msg in dB-buffer %d",dB_buffer_len(p_first));
		return;
	}
	// data saved correctly in dB: retrieve data if exist
	if(retrieve_data_from_dB_buffer(statement, &p_first, &p_last))   //at least one value to retreive
	{
		syslog( LOG_DEBUG, "Actual Nr of Raw Msg in dB-buffer %d",dB_buffer_len(p_first));
		if (mysql_query(&conn, statement))
		{
			finish_with_error(&conn);
			syslog( LOG_CRIT, " ERROR SAVING RAW DATA in DB");
			save_data_in_dB_buffer(statement, &p_first, &p_last);
			syslog( LOG_DEBUG, "Actual Nr of Raw Msg in dB-buffer %d",dB_buffer_len(p_first));
			return;
		}	
	}
	mysql_close(&conn);
     
}

void save_decoded_data_DB(char* Msg, DB_Info* Solarfocus, int DisplayFormattedData)
{
	MYSQL conn;
    mysql_init(&conn);
    static Saved_DB_String *p_first=NULL;
	static Saved_DB_String *p_last=NULL;
    
    char statement[DB_STR_MAX_LEN];
    
    snprintf(statement, 2048, "INSERT INTO Data VALUES (%s)", Msg);
    
    /* Connect to database */
	if (!mysql_real_connect(&conn, Solarfocus->DB_server, Solarfocus->DB_user, Solarfocus->DB_password, Solarfocus->DB_name, Solarfocus->DB_port, NULL, 0))
	{
		syslog( LOG_CRIT, " ERROR CONNECTING TO DB FOR DECODED DATA: ERROR CODE %u: %s",mysql_errno(&conn),mysql_error(&conn));
		save_data_in_dB_buffer(statement, &p_first, &p_last);
		syslog( LOG_DEBUG, "Actual Nr of Decoded Msg in dB-buffer %d",dB_buffer_len(p_first));
		return;
	}
	if (!DisplayFormattedData) printf("%s\n",Msg);
       
	if (mysql_query(&conn, statement))
	{
		finish_with_error(&conn);
		syslog( LOG_CRIT, " ERROR SAVING Decoded DATA in DB");
		save_data_in_dB_buffer(statement, &p_first, &p_last);
		syslog( LOG_DEBUG, "Actual Nr of Decoded Msg in dB-buffer %d",dB_buffer_len(p_first));
		return;
    }
	// data saved correctly in dB:  retrieve data if exist
	if(retrieve_data_from_dB_buffer(statement, &p_first, &p_last))   //at least one value to retreive
	{
		syslog( LOG_DEBUG, "Actual Nr of Decoded Msg in dB-buffer %d",dB_buffer_len(p_first));
		if (mysql_query(&conn, statement))
		{
			finish_with_error(&conn);
			syslog( LOG_CRIT, " ERROR SAVING Decoded DATA in DB");
			save_data_in_dB_buffer(statement, &p_first, &p_last);
			syslog( LOG_DEBUG, "Actual Nr of Decoded Msg in dB-buffer %d",dB_buffer_len(p_first));
			return;
		}	
	}   
    mysql_close(&conn);    
}



void Char_TO_Hex(unsigned char *Original_Msg, int Original_len, char *Output_Msg)
{
	int i;
	
	const char table[] = {'0','1','2','3','4','5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
	for(i=0; i< Original_len; i++)
	{
		*Output_Msg = table[(Original_Msg[i] & 0xF0) >> 4];
		 Output_Msg++;
		*Output_Msg = table[Original_Msg[i] & 0x0F];
		 Output_Msg++;
	}
	*Output_Msg = 0;
}


/*******************************************************/
/* Function that verify if any ON-OFF Data has changed */
/*******************************************************/

int ON_OFF_Data_Changed(unsigned short* Old_Values,unsigned short* DB_Values)
{
	int i;
	// verify the first ON_OFF_NR_OF_DATA Values if they has changed
	for (i=S1_O1_Pump_State; i <= ON_OFF_NR_OF_DATA; i++)
	{
		if (Old_Values[i] != DB_Values[i]) return(TRUE);
	}
	return(FALSE);
}


int main(int argc, char **argv)
{ 
	unsigned char master_msg[MaxMsgLen];
	unsigned char slave_msg[MaxMsgLen]; 
	char master_hex_msg[(MaxMsgLen+1)*2];
	char slave_hex_msg[(MaxMsgLen+1)*2]; 
	char DB_Values_string_msg [DB_STR_LEN];
	int master_len;
	int slave_len;
	int USB_PORT;
	int len_master;
	int len_slave;
	int first_time=TRUE;
	unsigned short DB_Values[MaxValues];
	unsigned short Old_Values[MaxValues];
	int Counter;
 	time_t start_t, end_t;
	int Counter_row_data=0;
	int n;
	int DB_Save_Time_interval;
	char USB_path[50];
	int SaveDataInDB;
	int SaveRawDataInDB;
	
	
	// OPEN SYSLOG
	openlog("Solarfocus_sniffer", LOG_CONS | LOG_PID, LOG_DAEMON);
	
	// save msg in logfile program started
	
	syslog( LOG_NOTICE, " Solarfocus Sniffer started");
	
	// MYSQL DATABASE INFO

	DB_Info Solarfocus_DB;

	
	// define witch data as to be printed
	int MAIN_BOARD =FALSE; //Mainboard
	int SOLAR_REGISTER_1_2 = FALSE; // Solar register 1
	int SOLAR_REGISTER_3 = FALSE;  // Solar register 2
	int FRESH_WATER = FALSE; // Freschwater
	int DISPLAY_DATA = FALSE; // Display decoded data on screen

	
	char stringa[100];


	
	SavedStruct Solar_Master_Msg[SavedStructSize];
	SavedStruct Solar_Slave1_Msg[SavedStructSize];
	SavedStruct Solar_Slave2_Msg[SavedStructSize];
	SavedStruct FreshWater_Master_Msg[SavedStructSize];
	SavedStruct FreshWater_Slave_Msg[SavedStructSize];
	SavedStruct Motherboard_Master_Msg[SavedStructSize];
	SavedStruct Motherboard_Slave_Msg[SavedStructSize];
	
	printf ("Verwendung der SW und HW (alle Info) auf eigene Gefahr Ich übernehme keine Haftung für enthaltene Fehler oder für Schäden, die im Zusammenhang mit der Verwendung der Software entstehen könnten.\n");
	printf ("Use of SW and HW (all info) at your own risk I assume no liability for errors contained or for damages that might occur in connection with the use of the software.\n");
	printf (" Akzeptieren Sie die Nutzungsbedingungen?, Do you accept the term of use?   YES/NO:   ");
	scanf("%s",stringa);
	if (( strcmp(stringa, "YES") == 0) ||  (strcmp(stringa, "yes") == 0));
	else 
	{
		syslog( LOG_NOTICE, " User didn't accept the term of use");
		exit (0);
	}
	
	
	// read ini file
	n = (int)ini_getl("verbose", "log_level", 0, inifile);
	setlogmask (LOG_UPTO(n));
	syslog( LOG_NOTICE, " Verbose level: %d",n);
	n = ini_gets("Database", "DB_server", "dummy", Solarfocus_DB.DB_server, sizeof(Solarfocus_DB.DB_server), inifile);
	//printf(" DB_server = %s\n", Solarfocus_DB.DB_server); 
	n = ini_gets("Database", "DB_name", "dummy", Solarfocus_DB.DB_name, sizeof(Solarfocus_DB.DB_name), inifile);
	//printf(" DB_name = %s\n", Solarfocus_DB.DB_name); 
	n = ini_gets("Database", "DB_user", "dummy", Solarfocus_DB.DB_user, sizeof(Solarfocus_DB.DB_user), inifile);
	//printf(" DB_user = %s\n", Solarfocus_DB.DB_user); 
	n = ini_gets("Database", "DB_password", "dummy", Solarfocus_DB.DB_password, sizeof(Solarfocus_DB.DB_password), inifile);
	//printf(" DB_password = %s\n", Solarfocus_DB.DB_password); 
	Solarfocus_DB.DB_port = (int)ini_getl("Database", "DB_port", 0000, inifile);
	//printf(" DB_port = %d\n", Solarfocus_DB.DB_port); 
	DB_Save_Time_interval = (int)ini_getl("Database", "DB_SAVE_FREQUENCY_TIME", 120, inifile);
	n = ini_gets("serial", "USB_dev", "dummy", USB_path, sizeof(USB_path), inifile);
	n = (int)ini_getl("display", "device_to_display", 0, inifile);
	
	switch (n)
	{
		case 0:
			DISPLAY_DATA = FALSE;
		break;
		case 1:
			DISPLAY_DATA = TRUE;
			MAIN_BOARD = TRUE;
		break;
		case 2:
			DISPLAY_DATA = TRUE;
			SOLAR_REGISTER_1_2 = TRUE;
		break;
		case 3:
			DISPLAY_DATA = TRUE;
			SOLAR_REGISTER_3 = TRUE;
		break;
		case 4:
			DISPLAY_DATA = TRUE;
			FRESH_WATER = TRUE;
		break;
		default:
			syslog( LOG_ERR, " Couldn't find the device to display");
		
	}
	
	SaveDataInDB = (int)ini_getl("Database", "DB_save", 0, inifile);
	if (!SaveDataInDB) syslog( LOG_WARNING, " No data will be stored in DB, To store data modify the configuration file");
	SaveRawDataInDB = (int)ini_getl("Database", "DB_RAW_SAVE", 0, inifile);
        if (!SaveRawDataInDB) syslog( LOG_WARNING, " No RAW data will be stored in DB, To store data modify the configuration file");
	
	// curs initialisation
	initscr(); 
	curs_set(0);   // no crusor

	/* open serial port and initialize it */

	//USB_PORT = open("/dev/ttyUSB0",O_RDONLY);
	USB_PORT = open(USB_path,O_RDONLY);
	if (USB_PORT == -1) 
	{
		syslog( LOG_CRIT, " ERROR OPENING RS422-USB PORT EXITING PROGRAM\n");
		endwin();
		return (0); 
	}
	else  syslog( LOG_INFO, " RS422-USB PORT opened");

	if (tty_raw(USB_PORT) < 0) 
	{
		syslog( LOG_CRIT, " error configuring RAW TTW EXITING PROGRAM");
		endwin();
		return (-1);
	}
 
	master_msg[0]=0x02;
	slave_msg[0]=0x02;
	slave_msg[1]=0x02;
	
	// start counting time for DB save DATA
	start_t=time(NULL);

	while(1)
	{
		
		 while ( getMsg(USB_PORT, master_msg)!= Master_ID);   // do nothing and wait for a master Msg
		 master_len =master_msg[1];
		 // get Msg delete the first 0x02 of the slave message leaving only one 0x02
		 // actual decoding functions need the whole slave message  with 2 times 0x02 
		 // so for compatibility reasons the extracted slave message will be saved starting from position 1 of the array so that 
		 // slave_msg[0] is still 0x02 and the msg can be elaborated without problems 
		 // also the len of the slave message will be increased of 1 for compatibility reasons
		 // in feature the decoding fuctions must be modified to accept the real slave message with only one 0x02
		 getMsg(USB_PORT, &slave_msg[1]); 
		 slave_len = slave_msg[2] +1;   

		
		/****************************************************************************/
		/* SAVE DATA IN DATABASE                                                    */
		/****************************************************************************/
		
		/* SAVE ROW DATA only every fith value and if data has to be stored */
		
		Char_TO_Hex(master_msg, master_len, master_hex_msg);
		
		if ((Counter_row_data % 125 == 0) && SaveDataInDB && SaveRawDataInDB)
		{
			//Char_TO_Hex(master_msg, master_len, master_hex_msg);
			Char_TO_Hex(slave_msg, slave_len, slave_hex_msg);
			save_row_data_DB(master_msg[2], master_hex_msg, slave_hex_msg, &Solarfocus_DB);
		}
		Counter_row_data++;
		
        /* save decoded data */
        
        /* save the last values in a temporary array so that they can be checked with the new ones */     
        for (Counter=0; Counter< MaxValues; Counter++)  Old_Values[Counter] = DB_Values[Counter];
        
        switch (master_msg[2])
        {
			case MainBoard:
				len_master = save_Motherboard_master_msg_data(master_msg, Motherboard_Master_Msg, DB_Values);
				len_slave  = save_Motherboard_slave_msg_data(slave_msg, Motherboard_Slave_Msg, DB_Values);		
			break;
			case SolarRegister_1_2:
				len_master = save_solar_master1_msg_data(master_msg, Solar_Master_Msg, DB_Values);
				len_slave = save_solar_slave1_msg_data(slave_msg, Solar_Slave1_Msg, DB_Values);
			break;
			case SolarRegister_3:
				len_master = save_solar_master2_msg_data(master_msg, Solar_Master_Msg, DB_Values);
				len_slave = save_solar_slave2_msg_data(slave_msg, Solar_Slave2_Msg, DB_Values);
			break;
			case FreshWater:
			    len_master = save_Freshwater_master_msg_data(master_msg, FreshWater_Master_Msg, DB_Values);
				len_slave = save_freshwater_slave_msg_data(slave_msg, FreshWater_Slave_Msg, DB_Values);
			break;
			default:
				syslog( LOG_ERR, "Master Msg not found in the list of acceptable Msg: %d",master_msg[2]);
			break;
		}
		
		/* save DB_Values-array in the DB if a digital signal (one or zero) has changed or if the elapsed tume as expired */ 
   
   
		/* get the actual time */
		end_t=time(NULL);
		
	
		/* if elapsed time is more than DB_SAVE_FREQUENCY_TIME second save data in DB or a ON-OFF Value in database has changed */
		if (((end_t-start_t) > DB_Save_Time_interval) || (ON_OFF_Data_Changed(Old_Values,DB_Values)))
		{
			if (SaveDataInDB)
			{
				//printf("SAVE DATA IN DB ............................\n");
				/* Create string Data */		
				if ((decoded_data_string(DB_Values, DB_Values_string_msg))>0)
					/* Save data in database */		
					save_decoded_data_DB(DB_Values_string_msg, &Solarfocus_DB, DISPLAY_DATA);
				else
				{
					// manage if the the string to save in the dB is not ok
					syslog( LOG_ERR, "string to save in the dB is not ok");
				}
			}

			/* save actual time in start time */
			start_t=time(NULL);
		}
		
	
      /*********************************************************************************************************************/
      /* PRINT FORMATTED DATA ON SCREEN                                                                               */
      /*********************************************************************************************************************/
      
      if(DISPLAY_DATA)
      {
			if (first_time)
			{
				first_time= FALSE;
				printHeader(0," MASTER       M E S S A G E");
				printHeader(70," SLAVE       M E S S A G E");
			}
			 switch (master_msg[2])
			{
				case MainBoard:  
					// PRINT MAIN BOARD
					if (MAIN_BOARD)
					{	
						formatted_print(Motherboard_Master_Msg, len_master, 0);						
						formatted_print(Motherboard_Slave_Msg, len_slave, 70);									
					}
				break;
				case SolarRegister_1_2:
					// PRINT SOLAR REGISTER 1
					if (SOLAR_REGISTER_1_2)
					{
						formatted_print(Solar_Master_Msg, len_master, 0);	
						formatted_print(Solar_Slave1_Msg, len_slave, 70);									
					}
				break;
				case SolarRegister_3:
				// PRINT SOLAR REGISTER 3
					if (SOLAR_REGISTER_3)
					{	 
						formatted_print(Solar_Master_Msg, len_master, 0);
						formatted_print(Solar_Slave2_Msg, len_slave, 70);									
					}
				break;
				case FreshWater:
					// PRINT FRESH WATER		
					if (FRESH_WATER)
					{ 
						formatted_print(FreshWater_Master_Msg, len_master, 0);
						formatted_print(FreshWater_Slave_Msg, len_slave, 80);	
						//printf("Fresh master%s\n",master_hex_msg);								
					}
				break;
				default:
					printf("Msg not found");
				break;
			}
		}
	}
	close(USB_PORT);
	endwin();
	return (0);
	
}

