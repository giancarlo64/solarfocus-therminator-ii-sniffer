#ifndef SOLARFOCUS
#define SOLARFOCUS

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

#define DescrLen 80
#define DB_STR_MAX_LEN 2048  // max len of the quesy MYSQL string

#define TRUE 1
#define FALSE 0

#define NANO_TO_MILLISECOND 1000000


#define VALVE_MASK 0x02
#define Pump_Solar1_MASK 0x01
#define PRIMARY_AIR_MASK 0x02
#define HOT_AIR_INJECTION_MASK 0x04
#define STANDARD_RETURN_PUMP_MASK 0x10
#define SECONDARY_AIR_ENABLE_MASK 0x02
#define SECONDARY_AIR_MASK 0x06  // bit 1 and 2
#define ASH_EXTRACTION_MASK 0x80
#define BOILER_MIXER_ENABLE_MASK 0x10
#define BOILER_MIXER_MASK 0x30 // bit 4 and 5

#define DB_STR_LEN (LAST_VALUE_FOR_INDEX+1)*7+26
#define LOG_FILE_STR_LEN  280



#define LITRI_IMPULSO 0.5
#define MIN(a,b) ((a) > (b)) ? (b) : (a)
#define column 80
#define offset1 12
#define offset2 (offset1+15)
#define SavedStructSize 100
#define MaxMsgLen 100
#define MaxValues 100
#define MaxAverageCounter 500
//#define MAX_FOW_SAME_VALUES 5 // max values after that the is now flow
#define MAX_TIME_FLOW 20
#define Master_ID 1
#define Slave_ID 2

#define save_1_byte_no_conversion(str,div,field) ({\
        data->Msg_Value_LSB = *bus_msg;\
        DB_val[field] = data->Msg_Value_LSB;\
        data->Real_Value = 0;\
	    data->divisor = div;\
	    strcpy(data->Description, str);\
	    data->un_valore = TRUE;\
	    data->conversione = FALSE;\
        bus_msg++;\
        data++;\
        msg_len++;\
})

#define save_1_byte_with_conversion(str,div,field) ({\
        data->Msg_Value_LSB = *bus_msg;\
        DB_val[field] = data->Msg_Value_LSB;\
        data->Real_Value = data->Msg_Value_LSB;\
	    data->divisor = div;\
	    strcpy(data->Description, str);\
	    data->un_valore = TRUE;\
	    data->conversione = TRUE;\
        bus_msg++;\
        data++;\
        msg_len++;\
})

#define save_2_byte_with_conversion(str,div,field) ({\
        data->Msg_Value_MSB = *bus_msg;\
	    bus_msg++;\
	    data->Msg_Value_LSB = *bus_msg;\
	    data->Real_Value = (short)(data->Msg_Value_MSB*256+data->Msg_Value_LSB);/* 10 times grater than real value= one decimal*/\
	    DB_val[field] = data->Real_Value;\
	    data->divisor = div;\
	    strcpy(data->Description, str);\
	    data->un_valore = FALSE;\
	    data->conversione = TRUE;\
        bus_msg++;\
        data++;\
        msg_len++;\
})

#define save_smoke_2_byte_with_conversion(str,div,field) ({\
        data->Msg_Value_MSB = *bus_msg;\
	    bus_msg++;\
	    data->Msg_Value_LSB = *bus_msg;\
        SmokeVal= (short)(data->Msg_Value_MSB*256+data->Msg_Value_LSB);/* 10 times grater than real value= one decimal*/\
	    data->Real_Value = (short)(4.072019051*pow(10,-16)*pow(SmokeVal,6)-2.544077*pow(10,-12)*pow(SmokeVal,5)+5.939593*pow(10,-9)*pow(SmokeVal,4)-6.5120612*pow(10,-6)*pow(SmokeVal,3)+0.00342353*pow(SmokeVal,2)+0.2247073*SmokeVal+53.956799);/* 10 times grater than real value= one decimal*/\
	    DB_val[field] = data->Real_Value;\
	    data->divisor = div;\
	    strcpy(data->Description, str);\
	    data->un_valore = FALSE;\
	    data->conversione = TRUE;\
        bus_msg++;\
        data++;\
        msg_len++;\
})


#define save_Checksum() ({\
    data->Msg_Value_LSB = *bus_msg;\
	data->Real_Value = 0;\
	strcpy(data->Description, "Checksum?");\
	data->un_valore = TRUE;\
	msg_len++;\
})

#define save_header_Master() ({\
    save_1_byte_no_conversion("Start Byte of Master Msg",1,0);\
	bus_msg_len = data->Msg_Value_LSB;  /* Read len before incrementig the pointers!*/\
	save_1_byte_no_conversion("Message length",1,0);\
	save_1_byte_no_conversion("Recipient Address of Msg",1,0);\
	save_1_byte_no_conversion("Incremental value start from 64 to 127",1,0);\
})

#define save_header_Slave() ({\
    save_1_byte_no_conversion("Start Byte of Slave Msg",1,0);\
	save_1_byte_no_conversion("Start Byte of Slave Msg",1,0);\
	bus_msg_len = data->Msg_Value_LSB; /* Read len before incrementig the pointers!*/ \
	save_1_byte_no_conversion("Message length",1,0);\
	save_1_byte_no_conversion("Recipient Address of Msg",1,0);\
	save_1_byte_no_conversion("Incremental value start from 64 to 127",1,0);\
	save_1_byte_no_conversion("allways 0x80",1,0);\
	save_1_byte_no_conversion("Master Command (Byte 5 of Master)",1,0);\
})

typedef struct SavedStruct
{
	unsigned char Msg_Value_MSB;
	unsigned char Msg_Value_LSB;
	int divisor;
	short Real_Value;
	char Description[DescrLen];
	char un_valore;
	char conversione;
}SavedStruct;

typedef struct DB_Info
{
	char  DB_server[20];
	char  DB_name[50];
	char  DB_user[50];
	char  DB_password[50];
	int   DB_port;
}DB_Info;




/**************************************************/
/* codification SLAVE                             */
/*   Mainboard    Nr ????                         */
/*   Solar Register 1+2   Bus-Addr 1              */
/*   Solar Register 3     Bus-Addr 2              */
/*   Fresh-water          Bus-Addr 5              */
/**************************************************/


typedef enum Therminator_II_Slave
{
	MainBoard=0x11,
	SolarRegister_1_2=0x13,
	SolarRegister_3=0x14,
	FreshWater=0x17,   
	
}Therminator_II_Slave;

typedef enum msgType
{
	NoMsg,
	Master,
	Slave,
	
}msgType;

/**************************************************************/
/*       CAUTION                                              */
/*  to insert new fields or move fields do as follows:        */
/*  Enter new field where you want to inquest in list         */
/*  Fields must be entered AFTER S1_O1_Pump_State             */
/*  and BEFORE LAST_VALUE_FOR_INDEX never at the extremes     */
/*  therefore add or move fields in the database so that      */
/* the sequence matches the list below                        */
/* The first value must be 1 because first in the datgabase   */
/* here is a timestamp                                        */
/*  Never move S1_O1_Pump_State and LAST_VALUE_FOR_INDEX      */
/*  nor change the value of S1_O1_Pump_State                  */   
/*                                                            */
/* If a new value is introduced that can take                 */
/* only ON-OFF values. This must be introduced                */
/* at the beginning and the constant must be updated          */
/* ON_OFF_NR_OF_DATA indicating how much data are present     */
/* at the beginning of the DB (serves for updating the DB     */ 
/**************************************************************/

#define ON_OFF_NR_OF_DATA 10

typedef enum DB_Names
{
	S1_O1_Pump_State=1,   // ON-OFF DATA
	S1_O2_Valve_State,    // ON-OFF DATA
	S2_O1_Pump_State,     // ON-OFF DATA
  	Fresh_Water_Flowing,  // ON-OFF DATA
        X26_primary_air_control, //  ON-OFF DATA
	X17_secondary_air_control,  // COLSING, STOP  , OPENING
	X4_hot_air_injection, // ON-OFF DATA
        X15_standard_return_pump, // ON-OFF DATA
	X13_boiler_return_mixer,   // CLOSING, STOP, OPENING
	X16_ash_extraction,    // ON-OFF DATA
	S1_OUT1_Pump_SET_Speed,
	S1_Pump_CURRENT_Speed,
    Flow_elapsed_time,
    Solar_instant_power,
	S1_I1,
	S1_I3,
	S1_I4,
	S1_I5,
	S1_I9,
	S2_OUT1_Pump_SET_Speed,
	S2_Pump_CURRENT_Speed,
	S2_I9,
	X34,
	X30,
	X31,
	X32,
	U_Heating_Trafo,
	I_Heating_Trafo,
	Tens_Risc_PWM,
	X35,
	X40,
	X38,
	X37,
	X36,
	X44,
	X39,
	X42,
	X37_1,
	U_Lambda,
	Fan_Speed,
	Fresh_Water_Sampling_Time,
	FW_P_of_PID,
	FW_I_of_PID,
	FW_D_of_PID,
    FW_DIV_P_of_PID,
    FW_DIV_I_of_PID,
    FW_DIV_D_of_PID,
    FW_LIMIT_I_of_PID,
	Fresh_Water_Temp_SetPoint,
	Fresh_Water_Pump_Manual_SET_Speed,
	Fresh_Water_OUT_Temperature,
	Fan_Control_Boiler,	
	LAST_VALUE_FOR_INDEX, //only for index purpose
}DB_Names;

int timespec2str(char *buf, unsigned int len, struct timespec *ts);

#endif
