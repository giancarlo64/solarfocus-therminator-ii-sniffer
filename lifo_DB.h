#ifndef LIFO_DB
#define LIFO_DB

typedef struct Saved_DB_String
{
    char string[DB_STR_MAX_LEN];
    struct Saved_DB_String *p_next;
    struct Saved_DB_String *p_prev;
}Saved_DB_String;

void save_data_in_dB_buffer(char* Msg, Saved_DB_String **p_first, Saved_DB_String **p_last);
int retrieve_data_from_dB_buffer(char* Msg, Saved_DB_String **p_first, Saved_DB_String **p_last);
int dB_buffer_len(Saved_DB_String *p_first);

#endif
