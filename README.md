
# SOLARFOCUS BUS SNIFFER

## Project Description
The following SW sniff data on RS485 Solarfocus Therminator BUS and visualize it on a WEB interface

the WEB output is:

![WEB GUI](images/solarfocus_WEB.jpg)

## 1) Required hardware

    a) Solarfocus Therminator II (not tested on other machines)

    b) Linux based machine like Raspberry PI (client) with USB interface

    c) RS422 to USB interface works best with galvanic isolation (I have used
    the TRP-C08 interface). Attention use only RS422 (no
    RS485 interface) so you can only connect RX + and RX-. Keep connection wires from RS422 interface to
    Therminator BUS as short as possible (max 3-4cm)

    d) Ethernet connection

    e) If you want to have the database and the web pages on another machine a second linux based machine (DB-WWW machine)


## 2) Required software

    a) On client: Linux with SSH enabled (remote access)

    b) On the DB-WWW Machine (if different):  LAMP and SSH

## 3) sniffer installation

    a) Download sources files  to the client (sftp)
    b) run Makefile
    c) Adapt config.ini to your needs
    d) Start the compiled program

## 4) DB and WWW installation

    a) create a database named with the same information as in the solarfocus_config.ini
    b) set username and password to access database in the init.php file (same as in solarfocus_config.ini)
    c) upload the structure provided in www directory
    d) access the page with:  http://YOUR-WWW.IP/index.php

## 5) schematics

The design is based on the following Solarfocus schematics

![Solarfocus Schematic](images/Th_II_BK-SPS2R-PSR-FWM-Solar-schema.jpg)

Schematic of the RS422 connection interface

![Solarfocus Schematic](images/verbindungsschema.jpg)

Physical connection of TRP-C08 interface to the RS485 BUS

![Solarfocus Schematic](images/solarfocus_rs485_2a.jpg)


## 6) IMPORTANT

Not all the data are decoded. If someone decodes more data,
Can tell me new data and I will update the program.

### Important 1
ONLY connect RX + and RX- from RS422. So you can avoid HW-moderate,
the Raspberry writes on the RS485 BUS.


### Important 2
Use of SW and HW (all info) at your own risk
I accept no liability for any errors contained or for damages that could
arising in connection with the use of this SW and HW
