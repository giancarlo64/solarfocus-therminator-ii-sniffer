-- phpMyAdmin SQL Dump
-- version 4.6.6deb4+deb9u1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Creato il: Dic 23, 2020 alle 08:26
-- Versione del server: 10.3.23-MariaDB-0+deb10u1
-- Versione PHP: 7.3.19-1~deb10u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Solarfocus`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `Data`
--

CREATE TABLE `Data` (
  `Time_Stamp` datetime(6) NOT NULL DEFAULT current_timestamp(6),
  `S1_O1` smallint(6) NOT NULL COMMENT 'State (ON-OFF) Solar pump Register 1+2 ',
  `S1_O2` smallint(6) NOT NULL COMMENT 'State (Register 1 or Register 2) Solar valve',
  `S2_O1` smallint(6) NOT NULL COMMENT 'State (ON-OFF) Solar pump Register 3',
  `FW_Flow` smallint(6) NOT NULL COMMENT 'State Fresh Water (ON-OFF) flowing detector',
  `X26` smallint(6) NOT NULL COMMENT 'Primary AIR control',
  `X17` smallint(6) NOT NULL COMMENT 'Secondary AIR control',
  `X4` smallint(6) NOT NULL COMMENT 'Hot AIR Injection',
  `X15` smallint(6) NOT NULL COMMENT 'Standard Retrurn Pump',
  `X13` smallint(6) NOT NULL COMMENT 'Boiler Return Mixer',
  `X16` smallint(6) NOT NULL COMMENT 'ash extraction',
  `S1_OUT1` smallint(6) NOT NULL COMMENT 'Pump speed SET value Registers 1+2',
  `S1_Speed` smallint(6) NOT NULL COMMENT 'Pump speed IS value Registers 1+2',
  `Solar_Flow_volume` smallint(6) NOT NULL COMMENT 'Solar Flow volume in liters',
  `Solar_Instant_Power` smallint(6) NOT NULL COMMENT 'Instant Solar produced Power',
  `S1_I1` smallint(6) NOT NULL COMMENT 'Solar Collector Temp.',
  `S1_I3` smallint(6) NOT NULL COMMENT 'Solar delivery temp.',
  `S1_I4` smallint(6) NOT NULL COMMENT '1st buffer lower solar register temp.',
  `S1_I5` smallint(6) NOT NULL COMMENT 'Solar return temp.',
  `S1_I9` smallint(6) NOT NULL COMMENT '1st buffer upper solar register temp',
  `S2_OUT1` smallint(6) NOT NULL COMMENT 'Pump speed SET value Register 3',
  `S2_Speed` smallint(6) NOT NULL COMMENT 'Pump speed IS value Register 3',
  `S2_I9` smallint(6) NOT NULL COMMENT '2nd buffer solar register temp',
  `X34` smallint(6) NOT NULL COMMENT 'Smoke Temp.',
  `X30` smallint(6) NOT NULL,
  `X31` smallint(6) NOT NULL COMMENT 'Boiler (kessel) temp.',
  `X32` smallint(6) NOT NULL COMMENT 'Rücklauf temp.',
  `U_Heating_Trafo` smallint(6) NOT NULL COMMENT 'Tensione trasformatore riscaldamento',
  `I_Heating_Trafo` smallint(6) NOT NULL COMMENT 'Corrente trasformatore riscaldamento',
  `Tens_Risc_PWM` smallint(6) NOT NULL COMMENT 'Tens_Risc_PWM',
  `X35` smallint(6) NOT NULL COMMENT '2nd buffer low temperature',
  `X40` smallint(6) NOT NULL,
  `X38` smallint(6) NOT NULL COMMENT 'Heating circuit flow temp.',
  `X37` smallint(6) NOT NULL,
  `X36` smallint(6) NOT NULL COMMENT '1st buffer lower temp.',
  `X44` smallint(6) NOT NULL COMMENT '1st buffer middle temp.',
  `X39` smallint(6) NOT NULL COMMENT '1st buffer upper temp.',
  `X42` smallint(6) NOT NULL COMMENT 'Esternal temp.',
  `X37_1` smallint(6) NOT NULL,
  `U_Lambda` smallint(6) NOT NULL COMMENT 'Voltage in mV',
  `Fan_Speed` smallint(6) NOT NULL,
  `FW_Sampling_Time` smallint(6) NOT NULL COMMENT 'Fresh Water Sample T of reg.',
  `FW_P_of_PID` smallint(6) NOT NULL COMMENT 'P Value of FreshWater PID reg.',
  `FW_I_of_PID` smallint(6) NOT NULL COMMENT 'I Value of FreshWater PID reg.',
  `FW_D_of_PID` smallint(6) NOT NULL COMMENT 'D Value of FreshWater PID reg.',
  `FW_DIV_P_of_PID` smallint(6) NOT NULL COMMENT 'P DIV Value of FreshWater PID reg.',
  `FW_DIV_I_of_PID` smallint(6) NOT NULL COMMENT 'I DIV Value of FreshWater PID reg.',
  `FW_DIV_D_of_PID` smallint(6) NOT NULL COMMENT 'D DIV Value of FreshWater PID reg.',
  `FW_LIMIT_I_of_PID` smallint(6) NOT NULL COMMENT 'I LIMIT Value of FreshWater PID reg.',
  `FW_Temp_SetPoint` smallint(6) NOT NULL COMMENT 'Fresh Water Out Temp Set Point',
  `FW_Pump_SET_Speed` smallint(6) NOT NULL COMMENT 'Fresh water MANUAL (touch)heating exchange pump SET speed',
  `FW_OUT_T` smallint(6) NOT NULL COMMENT 'Fresh Water Output temp.',
  `FAN_Control_boiler` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `Data`
--
ALTER TABLE `Data`
  ADD PRIMARY KEY (`Time_Stamp`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
