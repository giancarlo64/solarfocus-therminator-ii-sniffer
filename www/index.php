<?php
	session_start();
	include 'init.php';
        define("smokeTempHigh", 130);
	define("smokeTempLow", 50);
?>
<!DOCTYPE html>
<html lang="it">
<head>
                <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <meta name="description" content="">
                <meta name="author" content="GCD">

	<title> Solarfocus interface </title>

</head>
<body>
	<?php
	function create_graphics($DB_Value, $DB_connection,$fromDate,$toDate)
	{
		STATIC $count = 0;
		$count++;
		$divider=10;
		printf("var data$count = new google.visualization.DataTable();\n");
		printf("data$count.addColumn('datetime', 'Date');\n");
		printf("data$count.addColumn('number', '%s');\n",$DB_Value);
		if ($DB_Value == 'Solar_Instant_Power') $divider=1;
		$select = "select Time_Stamp, $DB_Value/$divider as $DB_Value FROM Data WHERE Time_Stamp >'".$fromDate."' AND Time_Stamp < '".$toDate."'";
		if ($result = $DB_connection->query($select))
		{
			while ($row = $result->fetch_assoc())
			{
				echo("data$count.addRow([new Date('".$row['Time_Stamp']."'), ".$row[$DB_Value]."]);");
				echo("\n");
			}
		}
		echo("var chart$count = new google.visualization.LineChart(document.getElementById('chart".$count."_level'));\n");
		echo("var options1 = {\n");
			echo("isStacked: true,\n");
			echo("legend: \n");
			echo("{\n");
				echo('position: "top",');
				echo('alignment: "end"');
			echo("},");
			echo("hAxis: ");
			echo("{");
				echo('format: "dd.MM.yyyy kk:mm"');
			echo("},");
			echo("vAxis:");
			echo("{ ");
				echo('title: "Grafico '.$count.'", ');
				echo("viewWindowMode:'explicit',\n");
			echo("}\n");
		echo("};\n");
		echo("chart$count.draw(data$count, options1);\n");
		return $count;
		
	}
	
	function compute_wood_load($DB_connection,$fromDate,$toDate)
	{
		$wood_count=0;
		$wood_already_on=0;
		$select = "select Time_Stamp, X34 FROM Data WHERE Time_Stamp >'".$fromDate."' AND Time_Stamp < '".$toDate."'";
		$result = $DB_connection->query($select);
		while ($row = $result->fetch_assoc())
		{
			if ($row["X34"]/10 > smokeTempHigh) 
			{
				if (!$wood_already_on)
				{
					$wood_count++;
					$wood_already_on=1;
				}
			}
			else if ($row["X34"]/10 < smokeTempLow) $wood_already_on=0;
		}
		return $wood_count;
	}
	
	
	?>
	<!-- <?php var_dump($_POST); ?> -->
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
	
	<form method='post'>         
		<input type="text" name="daterange" value="<?php echo $_POST["daterange"]?>" />
		<button type='submit' name='bw'> <span class='glyphicon glyphicon-backward' aria-hidden='true'></span> SUBMIT !</button> 
		<fieldset>
			<legend><font size="5"><b>Sensors</b></font></legend>
				<table style="width:100%">
					<tr>
						<th>Solar System</th>
						<th>Heating System</th>
						<th>Fresh Water</th>
						<th>Buffer</th>
						<th>Miscellaneous</th>
					</tr>
					<tr>
						<td><input type="checkbox" name="S1_I1" value="1"/> Solar collector temp. (S1_I1)</td>
						<td><input type="checkbox" name="X34" value="1"/> Smoke temp. (X34)</td>
						<td><input type="checkbox" name="FW_OUT_T" value="1"/> Fresh Water temp.</td>
						<td><input type="checkbox" name="All_Temperature" value="1"/> All buffer temp.</td>
						<td><input type="checkbox" name="U_Lambda" value="1"/> Lambda sensor voltage</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="S1_I3" value="1"/> Solar delivery temp. (S1_I3)</td>
						<td><input type="checkbox" name="X31" value="1"/> Wood Burner OUT temp (X31)(high)</td>
						<td><input type="checkbox" name="FW_Flow" value="1"/>Fesch Water Flowing detector (ON-OFF)</td>
						<td></td>
						<td><input type="checkbox" name="U_Heating_Trafo" value="1"/> Voltage Heating Trafo</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="S1_I5" value="1"/> Solar return temp. (S1_I5)</td>
						<td><input type="checkbox" name="X32" value="1"/> Wood Burner IN temp (X32)(low)</td>
						<td><input type="checkbox" name="FW_Pump_SET_Speed" value="1"/> Fresh Water Pumps SET Speed</td>
						<td></td>
						<td><input type="checkbox" name="I_Heating_Trafo" value="1"/> Current Heating Trafo</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="S1_I4" value="1"/> 1st buffer lower solar register temp. (S1_I4)</td>
						<td><input type="checkbox" name="X38" value="1"/> Heating circuit temp. (X38)</td>
						<td><input type="checkbox" name="FW_Temp_SetPoint" value="1"/> Fresh Water OUT temp. SET point</td>
						<td></td>
						<td><input type="checkbox" name="Tens_Risc_PWM" value="1"/> Voltage heating PWM</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="S1_I9" value="1"/> 1st buffer upper solar register temp. (S1_I9)</td>
						<td><input type="checkbox" name="X42" value="1"/> Outdoor temp. (X42)</td>
						<td><input type="checkbox" name="FW_Sampling_Time" value="1"/>Fesch Water sampling time regulation</td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td><input type="checkbox" name="S2_I9" value="1"/> 2nd buffer lower solar register temp. (S2_I9)</td>
						<td><input type="checkbox" name="X15" value="1"/> Back heating pump (X15)</td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td><input type="checkbox" name="S1_O1" value="1"/> State (ON-OFF) Solar pump Reg. 1+2 (1st buffer) (S1_O1)</td>
						<td><input type="checkbox" name="X4" value="1"/> Injection hot air (X4)</td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td><input type="checkbox" name="S1_Speed" value="1"/> Pump speed IS value Reg. 1+2 (1st buffer) (S1_Speed)</td>
						<td><input type="checkbox" name="X26" value="1"/> Primary air (X26)</td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td><input type="checkbox" name="S1_OUT1" value="1"/> Pump speed SET value Reg. 1+2 (1st buffer) (S1_OUT1)</td>
						<td><input type="checkbox" name="X17" value="1"/> Secondary air (X17)</td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td><input type="checkbox" name="S1_O2" value="1"/> State (Reg. 1 or Reg. 2) Solar valve (S1_O2)</td>
						<td><input type="checkbox" name="X13" value="1"/> Boiler return mixer (X13)</td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td><input type="checkbox" name="S2_O1" value="1"/> State (ON-OFF) Solar pump Reg. 3 (2nd buffer) (S2_O1)</td>
						<td><input type="checkbox" name="X16" value="1"/> Ash extraction (X16)</td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td><input type="checkbox" name="S2_Speed" value="1"/> Pump speed IS val. Reg. 3 (2nd buffer) (S2_Speed)</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td><input type="checkbox" name="S2_OUT1" value="1"/> Pump speed SET val. Reg. 3 (2nd buffer) (S2_OUT1)</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td><input type="checkbox" name="Solar_Flow_volume" value="1"/> Metering of the solar fluid speed (l/hour)</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td><input type="checkbox" name="Solar_Instant_Power" value="1"/> Solar produced Instant Power</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
				</table> 
				
		</fieldset>
		
		
	</form>
    
	<script>
	$(function() 
	{
		$('input[name="daterange"]').daterangepicker(
		{
			opens: 'right',
			"showDropdowns": true,
			ranges: {
				'Today': [moment(), moment()],
				'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Last 7 Days': [moment().subtract(6, 'days'), moment()],
				'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				'This Month': [moment().startOf('month'), moment().endOf('month')],
				'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')] },
				
				locale: {
					format: 'DD.MM.YYYY'
				},
				
		}, 
		
				function(start, end, label) 
		{
			console.log("A new date selection was made: " + start.format('DD-MM-YYYY') + ' to ' + end.format('DD-MM-YYYY'));
		});
	});
	</script>


	<script type='text/javascript'>
	
	google.charts.load('current', {'packages':['corechart']});
	google.charts.setOnLoadCallback(drawLineStyles);
	
      
		
	function drawLineStyles() 
	{
		<?php
			$NrOfCharts=0;
			$format = "d.m.Y";
			$firstDate= substr ( $_POST["daterange"] , 0, 10 );
			$lastDate = substr ( $_POST["daterange"] , -10, 10 );
			$fromDate = (substr( $firstDate , 6, 4 ) . "-" . substr( $firstDate , 3, 2 ) . "-" . substr( $firstDate , 0, 2 ) . "  00:00:00");
			$toDate =  (substr( $lastDate , 6, 4 ) . "-" . substr( $lastDate , 3, 2 ) . "-" . substr( $lastDate , 0, 2 ). "  23:59:59");
			
			$conn = mysqli_connect($db_host, $db_username, $db_password, $db_name);
			if (!$conn)
			{
				echo "Error: Unable to connect to DB" . PHP_EOL;
				echo "Debugging Errno: " . mysqli_connect_errno() . PHP_EOL;
				echo "Debugging Error: " . mysqli_connect_error() . PHP_EOL;
				exit;
			}
			
			// add fixed values to the POST
						
			if (isset($_POST["S1_I1"])) 
			{  
				$NrOfCharts=create_graphics('S1_I1', $conn,$fromDate,$toDate);
			};
				
			if (isset($_POST["S1_I3"])) 
			{  
				$NrOfCharts=create_graphics('S1_I3', $conn,$fromDate,$toDate);        
			}; 
			
			if (isset($_POST["S1_I5"])) 
			{  
				$NrOfCharts=create_graphics('S1_I5', $conn,$fromDate,$toDate);               
			};
			if (isset($_POST["S1_I4"])) 
			{  
				$NrOfCharts=create_graphics('S1_I4', $conn,$fromDate,$toDate);       
			};
			if (isset($_POST["S1_I9"])) 
			{  
				$NrOfCharts=create_graphics('S1_I9', $conn,$fromDate,$toDate);       
			};
			if (isset($_POST["S2_I9"])) 
			{  
				$NrOfCharts=create_graphics('S2_I9', $conn,$fromDate,$toDate);       
			};
			if (isset($_POST["S1_O1"])) 
			{  
				$NrOfCharts=create_graphics('S1_O1', $conn,$fromDate,$toDate);       
			};
			if (isset($_POST["S1_Speed"])) 
			{  
				$NrOfCharts=create_graphics('S1_Speed', $conn,$fromDate,$toDate);       
			};
			if (isset($_POST["S1_OUT1"])) 
			{  
				$NrOfCharts=create_graphics('S1_OUT1', $conn,$fromDate,$toDate);       
			};
			if (isset($_POST["S1_O2"])) 
			{  
				$NrOfCharts=create_graphics('S1_O2', $conn,$fromDate,$toDate);       
			};
			if (isset($_POST["S2_O1"])) 
			{  
				$NrOfCharts=create_graphics('S2_O1', $conn,$fromDate,$toDate);       
			};
			if (isset($_POST["S2_Speed"])) 
			{  
				$NrOfCharts=create_graphics('S2_Speed', $conn,$fromDate,$toDate);       
			};
			if (isset($_POST["S2_OUT1"])) 
			{  
				$NrOfCharts=create_graphics('S2_OUT1', $conn,$fromDate,$toDate);       
			};
			if (isset($_POST["Solar_Flow_volume"])) 
			{  
				$NrOfCharts=create_graphics('Solar_Flow_volume', $conn,$fromDate,$toDate);       
			};
			if (isset($_POST["Solar_Instant_Power"])) 
			{  
				$NrOfCharts=create_graphics('Solar_Instant_Power', $conn,$fromDate,$toDate);       
			};
			if (isset($_POST["X34"])) 
			{  
				$NrOfCharts=create_graphics('X34', $conn,$fromDate,$toDate);       
			};
			if (isset($_POST["X31"])) 
			{  
				$NrOfCharts=create_graphics('X31', $conn,$fromDate,$toDate);       
			};
			if (isset($_POST["X32"])) 
			{  
				$NrOfCharts=create_graphics('X32', $conn,$fromDate,$toDate);       
			};
			if (isset($_POST["X38"])) 
			{  
				$NrOfCharts=create_graphics('X38', $conn,$fromDate,$toDate);       
			};
			if (isset($_POST["X26"])) 
			{  
				$NrOfCharts=create_graphics('X26', $conn,$fromDate,$toDate);       
			};
			if (isset($_POST["X17"])) 
			{  
				$NrOfCharts=create_graphics('X17', $conn,$fromDate,$toDate);       
			};
			if (isset($_POST["X4"])) 
			{  
				$NrOfCharts=create_graphics('X4', $conn,$fromDate,$toDate);       
			};
			if (isset($_POST["X15"])) 
			{  
				$NrOfCharts=create_graphics('X15', $conn,$fromDate,$toDate);       
			};
			if (isset($_POST["X13"])) 
			{  
				$NrOfCharts=create_graphics('X13', $conn,$fromDate,$toDate);       
			};
			if (isset($_POST["X16"])) 
			{  
				$NrOfCharts=create_graphics('X16', $conn,$fromDate,$toDate);       
			};
			if (isset($_POST["X42"])) 
			{  
				$NrOfCharts=create_graphics('X42', $conn,$fromDate,$toDate);       
			};
			if (isset($_POST["FW_OUT_T"])) 
			{  
				$NrOfCharts=create_graphics('FW_OUT_T', $conn,$fromDate,$toDate);       
			};
			if (isset($_POST["FW_Flow"])) 
			{  
				$NrOfCharts=create_graphics('FW_Flow', $conn,$fromDate,$toDate);       
			};
			if (isset($_POST["FW_Pump_SET_Speed"])) 
			{  
				$NrOfCharts=create_graphics('FW_Pump_SET_Speed', $conn,$fromDate,$toDate);       
			};
			if (isset($_POST["FW_Temp_SetPoint"])) 
			{  
				$NrOfCharts=create_graphics('FW_Temp_SetPoint', $conn,$fromDate,$toDate);       
			};
			if (isset($_POST["FW_Sampling_Time"])) 
			{  
				$NrOfCharts=create_graphics('FW_Sampling_Time', $conn,$fromDate,$toDate);       
			};	
			if (isset($_POST["U_Lambda"])) 	
			{  
				$NrOfCharts=create_graphics('U_Lambda', $conn,$fromDate,$toDate);       
			};
			if (isset($_POST["U_Heating_Trafo"])) 	
			{  
				$NrOfCharts=create_graphics('U_Heating_Trafo', $conn,$fromDate,$toDate);       
			};	
			if (isset($_POST["I_Heating_Trafo"])) 	
			{  
				$NrOfCharts=create_graphics('I_Heating_Trafo', $conn,$fromDate,$toDate);       
			};
			if (isset($_POST["Tens_Risc_PWM"])) 	
			{  
				$NrOfCharts=create_graphics('Tens_Risc_PWM', $conn,$fromDate,$toDate);       
			};
			if (isset($_POST["All_Temperature"])) 
			{  
			?>
				var data100 = new google.visualization.DataTable();
					data100.addColumn('datetime', 'Date');
					data100.addColumn('number', 'X39');
					data100.addColumn('number', 'S1_I9');
					data100.addColumn('number', 'X44');
					data100.addColumn('number', 'S1_I4');
					data100.addColumn('number', 'X36');
					data100.addColumn('number', 'S2_I9');
					data100.addColumn('number', 'X35');
			<?php	
			
			
				$select = "select Time_Stamp, X39/10 as X39, S1_I9/10 as S1_I9, X44/10 as X44, S1_I4/10 as S1_I4, X36/10 as X36, S2_I9/10 as S2_I9, X35/10 as X35 FROM Data WHERE Time_Stamp >'".$fromDate."' AND Time_Stamp < '".$toDate."'";
				if ($res = $conn->query($select))
				{
					while ($row = $res->fetch_assoc())
					{
						echo("data100.addRow([new Date('".$row['Time_Stamp']."'), ".$row['X39'].", ".$row['S1_I9'].", ".$row['X44'].", ".$row['S1_I4'].", ".$row['X36'].", ".$row['S2_I9'].", ".$row['X35']."]);");
						echo("\n");
					}
				}
			?>
				var chart100 = new google.visualization.LineChart(document.getElementById('chart100_level'));
				var options100 = 
                {
					isStacked: true,
						legend: 
						{
							position: "top",
							alignment: "end"
						},
						hAxis: 
						{
							format: "dd.MM.yyyy kk:mm"
						},
						vAxis:
						{ 
							title: "Grafico 1", 
							viewWindowMode:'explicit',
                        }
                };
                chart100.draw(data100, options100);
			<?php                
			};
	?>
	}

<?php if(isset($_POST['<<'])){echo("btSel"); } ?> 

    	$(window).resize(function()
		{
     		drawLineStyles();
      	});

</script>

<!-- <?php var_dump($fromDate); ?> -->
<!--  <?php var_dump($toDate); ?> -->

<?php $select = "select FW_Sampling_Time, FW_P_of_PID, FW_I_of_PID, FW_D_of_PID, FW_DIV_P_of_PID, FW_DIV_I_of_PID, FW_DIV_D_of_PID, FW_LIMIT_I_of_PID, FW_Temp_SetPoint/10 as FW_Temp_SetPoint FROM Data ORDER BY Time_Stamp DESC LIMIT 1  ";
$res = $conn->query($select);
$row = $res->fetch_assoc();

$result=compute_wood_load($conn,$fromDate,$toDate);
?>
<table>
  <tr>
    <th>
<fieldset style="width:600px">
			<legend><font size="5"><b>Fixed Values</b></font></legend>
				<table>
                                    <tr>
                                      <td>
					<tr>
						<th style="width:130px">Solar System</th>
						<th style="width:130px">Heating System</th>
						<th style="width:130px">Fresh Water</th>
					</tr>
					<tr align ="left" style="font-weight: 400;">
						<td></td>
						<td></td>
						<td>FW_P_of_PID=<?php echo($row['FW_P_of_PID']);?> </td>
					</tr>
					<tr align ="left" style="font-weight: 400;">
						<td></td>
						<td></td>
						<td>FW_I_of_PID=<?php echo($row['FW_I_of_PID']);?> </td>
					</tr>
					<tr align ="left" style="font-weight: 400;">
						<td></td>
						<td></td>
						<td>FW_D_of_PID=<?php echo($row['FW_D_of_PID']);?> </td>
					</tr>
					<tr align ="left" style="font-weight: 400;">
						<td></td>
						<td></td>
						<td>FW_DIV_P_of_PID=<?php echo($row['FW_DIV_P_of_PID']);?> </td>
					</tr>
					<tr align ="left" style="font-weight: 400;">
						<td></td>
						<td></td>
						<td>FW_DIV_I_of_PID=<?php echo($row['FW_DIV_I_of_PID']);?> </td>
					</tr>
					<tr align ="left" style="font-weight: 400;">
						<td></td>
						<td></td>
						<td>FW_DIV_D_of_PID=<?php echo($row['FW_DIV_D_of_PID']);?> </td>
					</tr>
					<tr align ="left" style="font-weight: 400;">
						<td></td>
						<td></td>
						<td>FW_LIMIT_I_of_PID=<?php echo($row['FW_LIMIT_I_of_PID']);?> </td>
					</tr>
					<tr align ="left" style="font-weight: 400;">
						<td></td>
						<td></td>
						<td>FW_Temp_SetPoint=<?php echo($row['FW_Temp_SetPoint']);?> </td>
					</tr>
					<tr align ="left" style="font-weight: 400;">
						<td></td>
					</tr>
					<tr align ="left" style="font-weight: 400;">
						<td></td>
					</tr>
					<tr>
						<td></td>
					</tr>
					<tr>
						<td></td>
					</tr>
					<tr>
						<td></td>
					</tr>
                                      
                                     </td>
                                   </tr>
                                  <tr>
                                     <td>
                                     </td>
                                  </tr>
                                  <tr>
                                  </tr>
                                  <tr>
				    <td>
                                        <tr>
                                                <th style="width:130px">Buffer</th>
                                                <th style="width:200px">Miscellaneous</th>
                                                <th> </th>
                                        </tr>
                                        <tr align ="left" style="font-weight: 400;">
                                                <td></td>
                                                <td>Nr of Wood loads= <?php echo($result);?> </td>
                                        </tr>
                                        <tr>
                                                <td></td>
                                        </tr>
                                        <tr>
                                                <td></td>
                                        </tr>
                                        <tr>
                                                <td></td>
                                        </tr>
                                        <tr>
                                                <td></td>
                                        </tr>
                                    
                                     </td>
                                   </tr>

				</table> 
				
		</fieldset>
            </th>
            <th> <img src="solarfocus-schema.jpg" alt="" border=3 width=1200></img> </th>
</tr>
</table>
	
	<?php
	
	for($i=1; $i <= $NrOfCharts; $i++)
	{
		echo("<div id='chart".$i."_level' style='width: 100%; height: 400px;'></div>");
	}
	if (isset($_POST["All_Temperature"])) 
	{ 	
		echo("<div id='chart100_level' style='width: 100%; height: 400px;'></div>");
	}?>
</body>

