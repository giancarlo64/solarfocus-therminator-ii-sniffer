#include "solarfocus1.h"
#include "lifo_DB.h"
#include <string.h>
#include <stddef.h>
#include <stdlib.h>

/*********************************************************************/
/*                                                                   */
/*  Filename:  lifo_DB.c                                             */
/*  Author:    G.C.D.                                                */
/*  Version:   see gitlab                                            */
/*  Data:      24.12.2018                                            */
/*  -----------------------------------------------------------------*/
/*  A. Function:                                                     */
/*     Fifo for MSG when connection to dB is not present             */
/*                                                                   */
/*  B. References:                                                   */
/*     None                                                          */
/*                                                                   */
/*  C. Specifications:                                               */
/*     None                                                          */
/*                                                                   */
/*  D. Comment:                                                      */
/*                                                                   */
/*                                                                   */
/*  E. Modification History                                          */
/*     See gitlab                                                    */                                  
/*********************************************************************/

void save_data_in_dB_buffer(char* Msg, Saved_DB_String **p_first, Saved_DB_String **p_last)
{
	Saved_DB_String *p;
	p = (Saved_DB_String*) malloc(sizeof(Saved_DB_String));
	
	// copy data in the new structure
	strcpy(p->string, Msg);
	p->p_next=NULL;
	
	if (*p_first==NULL) //first data
	{
		*p_first=p;
		*p_last=p;
		p->p_prev=NULL;
	}
	else
	{
		(*p_last)->p_next=p;
		p->p_prev= *p_last;
		*p_last=p;
	}	
}

int retrieve_data_from_dB_buffer(char* Msg, Saved_DB_String **p_first, Saved_DB_String **p_last)
{
	//Saved_DB_String *p;
	
	if (*p_first==NULL) return 0; // no data to retrieve
	strcpy(Msg, (*p_last)->string);
	*p_last = (*p_last)->p_prev;
	if (*p_last == NULL)   // only one structure left
	{
		free(*p_first);
		*p_first=NULL;
	}
	else  
	{
		free((*p_last)->p_next);
		(*p_last)->p_next=NULL;
	}
	return 1;
}

int dB_buffer_len(Saved_DB_String *p_first)
{
	int i=1;
	if (p_first == NULL) return 0;
	while (p_first->p_next != NULL) 
	{
		p_first=p_first->p_next;
		i++;
	}
	return i;
}
